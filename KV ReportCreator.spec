# -*- mode: python ; coding: utf-8 -*-

block_cipher = None

added_files = [
         ( 'bin/Variance_Explanation.png', '.bin' ),
         ( 'bin/NO_IMAGE_PROVIDED.png', '.bin'),
         ( 'bin/ReportCreator_Small.ico', '.bin'),
         ( 'bin/ReportCreator_Large.ico', '.bin'),
         ( 'templates/Content_Mapper_TEMPLATE.xlsx', '.templates' ),
         ( 'templates/Kemp_Report_TEMPLATE.pptx', '.templates'),
         ( 'templates/Project_Info_TEMPLATE.xlsx', '.templates' ), 
         ('documentation/KV_ReportCreator_Walkthrough_v1.3.2.pdf', '.documentation'),
          ('example_data/*', '.example_data')
         ]

ICON_PATH = 'C:\\_Data\\KempReporting\\el_report_creator\\bin\\ReportCreator_Small.ico'


a = Analysis(['C:\\_Data\\KempReporting\\el_report_creator\\source\\ReportCreator_Controller.py'],
             pathex=['C:\\_Data\\KempReporting\\el_report_creator', 'C:\\_Data\\KempReporting\\el_report_creator\\source'],
             binaries=[],
             datas=added_files,
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='KV ReportCreator',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=False,
          icon=ICON_PATH)
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               upx_exclude=[],
               name='source')
