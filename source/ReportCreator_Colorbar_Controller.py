import sys
import os

# Set path for PySide 2 plugins
qtPluginPath = os.path.join(os.path.dirname(sys.executable), r'Lib\site-packages\PySide2\plugins')
os.environ['QT_QPA_PLATFORM_PLUGIN_PATH'] = qtPluginPath

from PySide2.QtWidgets import QDialog, QApplication, QStyleFactory, QColorDialog, QAbstractItemView, QHeaderView
from PySide2.QtCore import Qt, Signal, QThread
from PySide2.QtGui import QIcon, QColor, QPixmap
from PySide2 import QtGui

import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib.colors import LinearSegmentedColormap, ListedColormap

import numpy
import pandas

import KV_Imaging as kvimg

from ReportCreator_Colorbar_Window import Ui_Dialog as ColorbarUi

# Define module constants.
# Get info about this file.
if getattr(sys, 'frozen', False):
    # frozen
    SCRIPT_FULLPATH = sys.executable
else:
    # unfrozen
    SCRIPT_FULLPATH = __file__
SCRIPT_DIRECTORY = os.path.dirname(SCRIPT_FULLPATH)
SCRIPT_FILENAME = os.path.basename(SCRIPT_FULLPATH)

MAIN_DIRECTORY = os.path.dirname(SCRIPT_DIRECTORY)

# Public AppData folder. Windows users should have write access here.
APP_DATA_DIRECTORY = r'C:\Users\Public\AppData\KV_ReportCreator'
if not os.path.isdir(APP_DATA_DIRECTORY):
    os.makedirs(APP_DATA_DIRECTORY)


# Define user settings file.
SETTINGS_FILENAME = 'Report Creator Colorbar Settings.config'
SETTINGS_FULLPATH = os.path.join(APP_DATA_DIRECTORY, SETTINGS_FILENAME)

DEFAULT_MAX_NOMINAL = 0.50
DEFAULT_MAX_CRITICAL = 0.05
DEFAULT_MIN_CRITICAL = -0.05
DEFAULT_MIN_NOMINAL = -0.50
DEFAULT_FRINGE_COLORS_BOOL = True
DEFAULT_TICK_COUNT = 3

DEFAULT_MAX_COLOR = '#FF0099'
DEFAULT_MIDDLE_COLOR = '#00FF00'
DEFAULT_MIN_COLOR = '#9900FF'

DEFAULT_SCALE_PREVIEW = os.path.join(APP_DATA_DIRECTORY, 'scale_preview.png')
print(DEFAULT_SCALE_PREVIEW)


class ParameterWindow(QDialog, ColorbarUi):
    '''
    Class for the Colorbar Creator main application window.
    '''

    def __init__(self, logger=None, icon=None):
        # Initialize the parent classes.
        super().__init__()

        # Get Logging Window and Logger
        self.colorbarLogger = logger

        # Set Icon
        self.icon = icon

        # Initialize the widgets from the ui designer file.
        # NOTE: this adds all of our widgets from the ui file to the MainWindow class.
        self.setupUi(self)

        # Initialize window properties.
        self.init_window()

        # Initialize window settings.
        self.init_window_settings()

        # Apply settings for the window.
        self.apply_window_settings()

        # Load and apply settings from file.
        self.load_settings_from_file()

        # Initialize widget actions.
        self.init_widget_actions()

        # Set Color Scale Type to Gradient by default
        self.radioButtonDiscrete.toggle()

        # Set Spacing and Label
        self.calculate_spacing()


        return

    def init_window(self):
        # Set some options for the window.
        self.setWindowTitle('KV Report Creator - Colorbar Creator')
        self.setWindowIcon(QIcon(self.icon))
        self.setFixedSize(self.size())
        self.setWindowModality(Qt.ApplicationModal)

    def init_widget_actions(self):
        """ Menu Bar Actions """
        ### Edit Spectrum Type
        self.radioButtonDiscrete.toggled.connect(
            self.calculate_spacing
        )

        ### Edit Tick Count
        self.spinBoxTickCount.valueChanged.connect(
            self.check_spinbox_odd_entry
        )

        ### Edit Max Nominal
        self.doubleSpinBoxMaxNominal.valueChanged.connect(
            self.calculate_spacing
        )

        ### Edit Max Critical
        self.doubleSpinBoxMaxCritical.valueChanged.connect(
            self.calculate_spacing
        )

        ### Edit Min Critical
        self.doubleSpinBoxMinCritical.valueChanged.connect(
            self.calculate_spacing
        )

        ### Edit Min Nominal
        self.doubleSpinBoxMinNominal.valueChanged.connect(
            self.calculate_spacing
        )

        ### Change Segment Dropdown for Color
        self.comboBoxColorSegments.activated.connect(
            self.select_custom_color
        )  # currentIndexChanged

        """ Processing Start Action """
        self.pushButtonSave.clicked.connect(
            self.save_colorbar_selections
        )
        return

    def init_window_settings(self):
        # Create settings for window.
        self.settingsColorbar = kvimg.WindowSettings('MainWindow')

        # Controller Defaults
        self.settingsColorbar.maxNominalControllerDefault = DEFAULT_MAX_NOMINAL
        self.settingsColorbar.maxCriticalControllerDefault = DEFAULT_MAX_CRITICAL
        self.settingsColorbar.minCriticalControllerDefault = DEFAULT_MIN_CRITICAL
        self.settingsColorbar.minNominalControllerDefault = DEFAULT_MIN_NOMINAL
        self.settingsColorbar.colorSegCountDefault = DEFAULT_TICK_COUNT
        self.settingsColorbar.fringeColorsControllerDefault = DEFAULT_FRINGE_COLORS_BOOL

        self.settingsColorbar.maxColorControllerDefault = DEFAULT_MAX_COLOR
        self.settingsColorbar.midColorControllerDefault = DEFAULT_MIDDLE_COLOR
        self.settingsColorbar.minColorControllerDefault = DEFAULT_MIN_COLOR

        # Controller User Inputs
        self.settingsColorbar.maxNominalControllerUser = None
        self.settingsColorbar.maxCriticalControllerUser = None
        self.settingsColorbar.minCriticalControllerUser = None
        self.settingsColorbar.minNominalControllerUser = None
        self.settingsColorbar.colorSegCountUser = None
        self.settingsColorbar.fringeColorsControllerUser = None

        self.settingsColorbar.maxColorControllerUser = None
        self.settingsColorbar.midColorControllerUser = None
        self.settingsColorbar.minColorControllerUser = None

        self.settingsColorbar.colorRangeTypeController = None

        # Store Colormap and Norm Objects for colorbar
        self.settingsColorbar.colorMap = None
        self.settingsColorbar.colorNorm = None
        self.settingsColorbar.colorList = None

        self.settingsColorbar.spacing = None
        self.settingsColorbar.numColors = None

        self.settingsColorbar.colorSegmentInfo = dict()
        self.settingsColorbar.colorDict = {'max': DEFAULT_MAX_COLOR,
                                           'mid': DEFAULT_MIDDLE_COLOR,
                                           'min': DEFAULT_MIN_COLOR}

        self.previewScaleFile = DEFAULT_SCALE_PREVIEW
        return

    def apply_window_settings(self):

        """ Apply setting to widgets for the window."""
        # Widget: doubleSpinBoxMaxNominal
        if self.settingsColorbar.maxNominalControllerUser is not None and isinstance(
                self.settingsColorbar.maxNominalControllerUser, float):
            self.doubleSpinBoxMaxNominal.setValue(
                self.settingsColorbar.maxNominalControllerUser)
        else:
            self.doubleSpinBoxMaxNominal.setValue(
                self.settingsColorbar.maxNominalControllerDefault)

        # Widget: doubleSpinBoxMaxCritical
        if self.settingsColorbar.maxCriticalControllerUser is not None and isinstance(
                self.settingsColorbar.maxCriticalControllerUser, float):
            self.doubleSpinBoxMaxCritical.setValue(
                self.settingsColorbar.maxCriticalControllerUser)
        else:
            self.doubleSpinBoxMaxCritical.setValue(
                self.settingsColorbar.maxCriticalControllerDefault)

        # Widget: doubleSpinBoxMinCritical
        if self.settingsColorbar.minCriticalControllerUser is not None and isinstance(
                self.settingsColorbar.minCriticalControllerUser, float):
            self.doubleSpinBoxMinCritical.setValue(
                self.settingsColorbar.minCriticalControllerUser)
        else:
            self.doubleSpinBoxMinCritical.setValue(
                self.settingsColorbar.minCriticalControllerDefault)

        # Widget: doubleSpinBoxMinNominal
        if self.settingsColorbar.minNominalControllerUser is not None and isinstance(
                self.settingsColorbar.minNominalControllerUser, float):
            self.doubleSpinBoxMinNominal.setValue(
                self.settingsColorbar.minNominalControllerUser)
        else:
            self.doubleSpinBoxMinNominal.setValue(
                self.settingsColorbar.minNominalControllerDefault)

        # Widget: spinBoxTickCount
        if self.settingsColorbar.colorSegCountUser is not None and isinstance(
                self.settingsColorbar.colorSegCountUser, int):
            self.spinBoxTickCount.setValue(
                self.settingsColorbar.colorSegCountUser)
        else:
            self.spinBoxTickCount.setValue(
                self.settingsColorbar.colorSegCountDefault)

        if self.settingsColorbar.colorRangeTypeController == "gradient":
            self.radioButtonGradient.setChecked(True)

        if self.settingsColorbar.colorRangeTypeController == "discrete":
            self.radioButtonDiscrete.setChecked(True)

        # Widget:
        if self.settingsColorbar.colorMap is None and self.settingsColorbar.colorNorm is None:
            self.settingsColorbar.maxColorControllerUser = self.settingsColorbar.maxColorControllerDefault
            self.settingsColorbar.minColorControllerUser = self.settingsColorbar.minColorControllerDefault
            self.settingsColorbar.midColorControllerUser = self.settingsColorbar.midColorControllerDefault

        return

    def store_window_settings(self):

        """ Retrieve the settings from the widgets for the window."""

        self.settingsColorbar.maxNominalControllerUser = self.doubleSpinBoxMaxNominal.value()

        self.settingsColorbar.maxCriticalControllerUser = self.doubleSpinBoxMaxCritical.value()

        self.settingsColorbar.minCriticalControllerUser = self.doubleSpinBoxMinCritical.value()

        self.settingsColorbar.minNominalControllerUser = self.doubleSpinBoxMinNominal.value()

        self.settingsColorbar.colorSegCountUser = self.spinBoxTickCount.value()

        # Widget: comboBoxColorSegments
        self.settingsColorbar.userSegmentSelected = self.comboBoxColorSegments.currentIndex()

        # Get Processing Type
        self.check_spectrum_type()

        return

    def load_settings_from_file(self):
        # Load the settings from file.
        if os.path.isfile(SETTINGS_FULLPATH):
            self.settingsColorbar = kvimg.load_settings_file(SETTINGS_FULLPATH)
            self.apply_window_settings()
            # self.colorbarLogger.info('Colorbar Settings Loaded From File.')
            # # Apply settings for parameter window.
            # self.parameterWindow.settingsParameter = settingsAll.settingsParameter
            # self.parameterWindow.apply_window_settings()
        return

    def save_settings_to_file(self):
        # Get the latest settings.
        self.store_window_settings()
        # Save the settings file.
        kvimg.save_settings_file(SETTINGS_FULLPATH, self.settingsColorbar)
        # self.colorbarLogger.info('Colorbar Settings Saved To File.')
        return

    def closeEvent(self, event):
        # Get latest settings and save settings to file.
        # self.colorbarLogger.info('Colorbar Settings Closed.')
        return

    def check_spectrum_type(self):
        if self.radioButtonGradient.isChecked():
            # print ("Gradient Spectrum")
            self.settingsColorbar.colorRangeTypeController = "gradient"
        elif self.radioButtonDiscrete.isChecked():
            # print("Discrete Spectrum")
            self.settingsColorbar.colorRangeTypeController = "discrete"
        else:
            # print("Discrete Spectrum by default")
            self.settingsColorbar.colorRangeTypeController = "discrete"

    def enable_widgets(self, bool):
        self.doubleSpinBoxMaxCritical.setEnabled(bool)
        self.doubleSpinBoxMinCritical.setEnabled(bool)
        self.doubleSpinBoxMaxNominal.setEnabled(bool)
        self.doubleSpinBoxMinNominal.setEnabled(bool)
        self.radioButtonGradient.setEnabled(bool)
        self.radioButtonDiscrete.setEnabled(bool)
        self.pushButtonSave.setEnabled(bool)
        return

    def check_spinbox_odd_entry(self):
        enteredValue = self.spinBoxTickCount.value()
        if enteredValue % 2 != 1:
            # Even Value, subtracting 1 to get odd value
            oddValue = enteredValue - 1
            self.spinBoxTickCount.setValue(oddValue)
        self.calculate_spacing()
        return

    def calculate_spacing(self):

        self.store_window_settings()
        self.enable_widgets(True)

        scaleCenter = (self.settingsColorbar.maxCriticalControllerUser +
                       self.settingsColorbar.minCriticalControllerUser) / 2.0

        # subtract 1 for middle color, split into halves for other colors
        numColors = (self.settingsColorbar.colorSegCountUser - 1) / 2
        spacing = round(
            (float(self.settingsColorbar.maxNominalControllerUser) -
             float(self.settingsColorbar.maxCriticalControllerUser))
            / numColors, 4)

        self.labelSpacingValue.setValue(spacing)
        upperBound = numpy.linspace(self.settingsColorbar.maxNominalControllerUser,
                                    self.settingsColorbar.maxCriticalControllerUser,
                                    int(numColors) + 1).round(6)
        lowerBound = numpy.linspace(self.settingsColorbar.minCriticalControllerUser,
                                    self.settingsColorbar.minNominalControllerUser,
                                    int(numColors) + 1).round(6)
        centerBound = [round(self.settingsColorbar.maxCriticalControllerUser, 6),
                       round(self.settingsColorbar.minCriticalControllerUser, 6)]

        self.settingsColorbar.bounds = list(upperBound) + centerBound + list(lowerBound)
        self.settingsColorbar.bounds = sorted(list(set(self.settingsColorbar.bounds)))

        self.update_color_list()

        return

    def get_colorbar_info(self):

        if self.settingsColorbar.colorRangeTypeController == "gradient":
            self.settingsColorbar.colorMap = LinearSegmentedColormap.from_list('customScale',
                                                                               self.settingsColorbar.colorList,
                                                                               N=(len(
                                                                                   self.settingsColorbar.colorList) * 1000))
            # self.settingsColorbar.colorMap = ListedColormap(self.settingsColorbar.colorList, 'customScale',
            #                                                 N=len(self.settingsColorbar.colorList)*1000)
            self.settingsColorbar.colorNorm = mpl.colors.Normalize(vmin=min(self.settingsColorbar.bounds),
                                                                   vmax=max(self.settingsColorbar.bounds))
            # self.settingsColorbar.colorNorm = mpl.colors.BoundaryNorm(self.settingsColorbar.bounds,
            #                                                           self.settingsColorbar.colorMap.N)
            # cmap = ListedColormap(colorList, 'customScale', N=(len(colorList)*1000))
            # norm = mpl.colors.Normalize(vmin=self.settingsColorbar.bounds[0], vmax=self.settingsColorbar.bounds[-1])
            # norm = mpl.colors.BoundaryNorm(self.settingsColorbar.bounds, cmap.N)
            self.settingsColorbar.edges = False
        else:
            # cmap = LinearSegmentedColormap.from_list('customScale', self.colorList, N=len(self.colorList))
            self.settingsColorbar.colorMap = ListedColormap(self.settingsColorbar.colorList, 'customScale',
                                                            N=len(self.settingsColorbar.colorList))
            self.settingsColorbar.colorNorm = mpl.colors.BoundaryNorm(self.settingsColorbar.bounds,
                                                                      self.settingsColorbar.colorMap.N)
            self.settingsColorbar.edges = True

        # darkRed = numpy.array([139, 0, 0])
        # darkRedNormed = (darkRed / 255).tolist()
        self.settingsColorbar.colorMap.set_over('white')

        # darkBlue = numpy.array([0, 0, 139])
        # darkBlueNomred = (darkBlue / 255).tolist()
        self.settingsColorbar.colorMap.set_under('white')

        tempColorMap = ListedColormap(self.settingsColorbar.colorList, 'customScale',
                                                        N=len(self.settingsColorbar.colorList))
        tempColorNorm = mpl.colors.BoundaryNorm(self.settingsColorbar.bounds,
                                                                  self.settingsColorbar.colorMap.N)

        self.settingsColorbar.colorList = tempColorMap.colors
        self.settingsColorbar.bounds = tempColorNorm.boundaries
        return

    def update_color_list(self):
        if self.settingsColorbar.colorNorm is None and self.settingsColorbar.colorMap is None:
            self.settingsColorbar.colorList = [self.settingsColorbar.minColorControllerUser,
                                               self.settingsColorbar.midColorControllerUser,
                                               self.settingsColorbar.maxColorControllerUser]

        self.get_colorbar_info()

        maxColor = self.settingsColorbar.colorList[0]
        minColor = self.settingsColorbar.colorList[-1]

        # segColorDiff = int(
        #     (
        #             self.settingsColorbar.colorSegCountUser - len(self.settingsColorbar.colorList)
        #     ) / 2)

        segColorDiff = int((self.settingsColorbar.colorSegCountUser - len(self.settingsColorbar.colorList)) / 2)

        for diffCounter in list(range(abs(segColorDiff))):
            # Add Segments
            if segColorDiff > 0:
                # # Add first color to beginning
                # for addColorIdx in range(segColorDiff):
                self.settingsColorbar.colorList.insert(0, maxColor)
                # # Add first color to beginning
                # for addColorIdx in range(segColorDiff):
                self.settingsColorbar.colorList.append(minColor)
            elif segColorDiff < 0:
                self.settingsColorbar.colorList.pop(0)
                self.settingsColorbar.colorList.pop(-1)

        self.update_colorbar_image()

        mainBounds = pandas.Series(self.settingsColorbar.bounds)
        shiftBounds = pandas.Series(self.settingsColorbar.bounds).shift(-1)
        self.boundsConcat = pandas.concat([mainBounds, shiftBounds], axis=1).dropna()
        self.boundsConcat.loc[:, 'Intervals'] = self.boundsConcat.loc[:, 1].astype(str) + ' to ' + self.boundsConcat.loc[:, 0].astype(str)
        self.boundsConcat.loc[:, 'Colors'] = self.settingsColorbar.colorList
        self.boundsConcat = self.boundsConcat.sort_values(by=[1], inplace=False, ascending=False)

        self.set_combo_box_choices()
        return

    def update_colorbar_image(self):
        fig = plt.figure()
        ax = fig.add_axes([0.475, 0.1, 0.05, 0.8])  # left, bottom, width, height

        self.get_colorbar_info()

        cb2 = mpl.colorbar.ColorbarBase(ax, cmap=self.settingsColorbar.colorMap, norm=self.settingsColorbar.colorNorm,
                                        ticks=self.settingsColorbar.bounds, drawedges=self.settingsColorbar.edges,
                                        spacing='proportional',
                                        orientation='vertical', format='%0.4f')

        ax.yaxis.tick_left()
        # plt.show()
        self.previewScaleFile = os.path.join(APP_DATA_DIRECTORY, 'scale_preview.png')
        fig.savefig(self.previewScaleFile, bbox_inches='tight')
        plt.close(fig)
        self.labelImageExample.setPixmap(QPixmap(self.previewScaleFile))

        return

    def set_combo_box_choices(self):
        # colorList.reverse()
        self.comboBoxColorSegments.clear()
        self.settingsColorbar.colorSegmentInfo = dict()
        # boundsConcat.reset_index(inplace=True, drop=True)
        # self.settingsColorbar.colorList = boundsConcat.loc[:, 'Colors'].values.tolist()
        comboBoxModel = self.comboBoxColorSegments.model()
        # colorListReverse = copy.copy(self.settingsColorbar.colorList)
        # colorListReverse.reverse()
        # for colorIdx, colorVal in enumerate(colorListReverse):
        #     segmentName = "Segment %s" % str(int(colorIdx) + 1)
        # totalEntries = len(boundsConcat)
        for rowIdx, rowSeries in self.boundsConcat.iterrows():
            segmentName = rowSeries['Intervals']
            colorVal = rowSeries['Colors']
            item = QtGui.QStandardItem(str(rowIdx))
            item.setBackground(QtGui.QColor(colorVal))
            item.setText(segmentName)
            comboBoxModel.appendRow(item)
            # self.settingsColorbar.colorList[rowIdx] = colorVal
            # self.comboBoxColorSegments.addItem(segmentName)
        # comboBoxModel.sort(0)
        for boundIdx, color in enumerate(self.settingsColorbar.colorList):
            self.settingsColorbar.colorSegmentInfo[boundIdx] = color
        return

    def select_custom_color(self):
        self.settingsColorbar.userSegmentSelected = self.comboBoxColorSegments.currentIndex()
        color = QColorDialog.getColor()
        if color.isValid():
            colorHex = color.name()
            self.boundsConcat.iloc[self.settingsColorbar.userSegmentSelected, -1] = colorHex
            self.boundsConcat.sort_index(inplace=True)
            self.settingsColorbar.colorList = self.boundsConcat.loc[:, 'Colors'].values.tolist()
            self.update_color_list()
        return

    def save_colorbar_selections(self):
        # Disable the main gui while processing.
        # self.setEnabled(False)
        # self.start_thread_widget_status(False)

        # self.colorbarLogger.info('Storing Colorbar Settings.')

        # Get Parameters
        self.store_window_settings()

        # Get latest settings and save settings to file.
        self.save_settings_to_file()

        # Close
        self.close()

        return


if __name__ == '__main__':
    # Add support for multiprocessing for when code has been frozen as Windows executable.
    # https://docs.python.org/2/library/multiprocessing.html#miscellaneous
    # freeze_support()

    # Enable dpi scaling so if the windows system setting for scale is large than 100%
    # the application window scales accordingly without getting distorted.
    QApplication.setAttribute(Qt.AA_EnableHighDpiScaling, on=True)

    # Start the application.
    print('\nApplication started.')
    app = QApplication(sys.argv)

    # Set the main window style. Use windowsvista for newest windows style.
    # print(QStyleFactory.keys())
    app.setStyle(QStyleFactory.create('windowsvista'))
    # app.setStyle(QStyleFactory.create('Windows'))
    # app.setStyle(QStyleFactory.create('Fusion'))

    # Initialize and show the main window.
    mainGui = ParameterWindow()
    mainGui.show()
    app.exit(app.exec_())
    print('\nApplication closed.')
