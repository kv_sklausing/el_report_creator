# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'ReportCreator_Settings_Window.ui'
##
## Created by: Qt User Interface Compiler version 5.15.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.resize(583, 189)
        self.groupBoxPWSettings = QGroupBox(Dialog)
        self.groupBoxPWSettings.setObjectName(u"groupBoxPWSettings")
        self.groupBoxPWSettings.setGeometry(QRect(10, 10, 571, 131))
        font = QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.groupBoxPWSettings.setFont(font)
        self.lineEditPptTemplate = QLineEdit(self.groupBoxPWSettings)
        self.lineEditPptTemplate.setObjectName(u"lineEditPptTemplate")
        self.lineEditPptTemplate.setEnabled(False)
        self.lineEditPptTemplate.setGeometry(QRect(190, 30, 371, 30))
        font1 = QFont()
        font1.setPointSize(8)
        font1.setBold(True)
        font1.setWeight(75)
        self.lineEditPptTemplate.setFont(font1)
        self.pushButtonPptTemplate = QPushButton(self.groupBoxPWSettings)
        self.pushButtonPptTemplate.setObjectName(u"pushButtonPptTemplate")
        self.pushButtonPptTemplate.setEnabled(True)
        self.pushButtonPptTemplate.setGeometry(QRect(10, 30, 171, 31))
        font2 = QFont()
        font2.setPointSize(8)
        font2.setBold(False)
        font2.setWeight(50)
        self.pushButtonPptTemplate.setFont(font2)
        self.pushButtonInvalidImage = QPushButton(self.groupBoxPWSettings)
        self.pushButtonInvalidImage.setObjectName(u"pushButtonInvalidImage")
        self.pushButtonInvalidImage.setEnabled(True)
        self.pushButtonInvalidImage.setGeometry(QRect(10, 80, 171, 31))
        self.pushButtonInvalidImage.setFont(font2)
        self.lineEditInvalidImage = QLineEdit(self.groupBoxPWSettings)
        self.lineEditInvalidImage.setObjectName(u"lineEditInvalidImage")
        self.lineEditInvalidImage.setEnabled(False)
        self.lineEditInvalidImage.setGeometry(QRect(190, 80, 371, 30))
        self.lineEditInvalidImage.setFont(font1)
        self.pushButtonSetParameters = QPushButton(Dialog)
        self.pushButtonSetParameters.setObjectName(u"pushButtonSetParameters")
        self.pushButtonSetParameters.setGeometry(QRect(470, 150, 101, 28))
        self.pushButtonCancelParameters = QPushButton(Dialog)
        self.pushButtonCancelParameters.setObjectName(u"pushButtonCancelParameters")
        self.pushButtonCancelParameters.setGeometry(QRect(360, 150, 101, 28))
        QWidget.setTabOrder(self.pushButtonPptTemplate, self.lineEditPptTemplate)
        QWidget.setTabOrder(self.lineEditPptTemplate, self.pushButtonCancelParameters)
        QWidget.setTabOrder(self.pushButtonCancelParameters, self.pushButtonSetParameters)

        self.retranslateUi(Dialog)

        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Settings", None))
        self.groupBoxPWSettings.setTitle(QCoreApplication.translate("Dialog", u"Templates", None))
#if QT_CONFIG(tooltip)
        self.lineEditPptTemplate.setToolTip("")
#endif // QT_CONFIG(tooltip)
        self.lineEditPptTemplate.setText(QCoreApplication.translate("Dialog", u"C:\\Input\\Directory", None))
#if QT_CONFIG(tooltip)
        self.pushButtonPptTemplate.setToolTip(QCoreApplication.translate("Dialog", u"<html><head/><body><p>PowerPoint template used for creating the project report.</p></body></html>", None))
#endif // QT_CONFIG(tooltip)
        self.pushButtonPptTemplate.setText(QCoreApplication.translate("Dialog", u"Select Powerpoint Template", None))
#if QT_CONFIG(tooltip)
        self.pushButtonInvalidImage.setToolTip(QCoreApplication.translate("Dialog", u"<html><head/><body><p>Invalid image file used in report if there is a content mapping discrepancy.</p></body></html>", None))
#endif // QT_CONFIG(tooltip)
        self.pushButtonInvalidImage.setText(QCoreApplication.translate("Dialog", u"Select Invalid Image File", None))
#if QT_CONFIG(tooltip)
        self.lineEditInvalidImage.setToolTip("")
#endif // QT_CONFIG(tooltip)
        self.lineEditInvalidImage.setText(QCoreApplication.translate("Dialog", u"C:\\Input\\Directory", None))
        self.pushButtonSetParameters.setText(QCoreApplication.translate("Dialog", u"Save", None))
        self.pushButtonCancelParameters.setText(QCoreApplication.translate("Dialog", u"Cancel", None))
    # retranslateUi

