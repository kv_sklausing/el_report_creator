import sys
import os

from PySide2.QtWidgets import QDialog, QApplication, QStyleFactory
from PySide2.QtCore import Qt
from PySide2.QtGui import QIcon

from ReportCreator_Complete_Window import Ui_DialogComplete as CompleteWindowUi

if getattr(sys, 'frozen', False):
    # frozen
    SCRIPT_FULLPATH = sys.executable
else:
    # unfrozen
    SCRIPT_FULLPATH = __file__
SCRIPT_DIRECTORY = os.path.dirname(SCRIPT_FULLPATH)
SCRIPT_FILENAME = os.path.basename(SCRIPT_FULLPATH)

MAIN_DIRECTORY = os.path.dirname(SCRIPT_DIRECTORY)

class CompleteWindow(QDialog, CompleteWindowUi):
    '''
    Class for the Report Creator complete sub window.
    '''

    def __init__(self, logger=None, logWindow=None, icon=None):
        # Initialize the parent classes.
        super().__init__()


        # Get Logging Window and Logger
        self.completeLogger = logger

        self.logWindow = logWindow

        # Set Icon
        self.icon = icon

        # Initialize the widgets from the ui designer file.
        # NOTE: this adds all of our widgets from the ui file to the ParameterWindow class.
        self.setupUi(self)

        # Initialize window properties.
        self.init_window()

        # Initialize widget actions.
        self.init_widget_actions()
        return

    def init_window(self):
        # Set some options for the window.
        self.setWindowTitle('Process Complete.')
        self.label.setText('Process Complete.')
        self.setWindowIcon(QIcon(self.icon))
        self.setFixedSize(self.size())
        self.setWindowModality(Qt.ApplicationModal)

        return

    def init_widget_actions(self):
        # Connect the widgets to their actions.
        """ Okay Defaults Action """
        self.pushButtonComplete.clicked.connect(
            self.exit_window
        )

        """"""
        self.pushButtonViewSummary.clicked.connect(
            self.view_log_action
        )
        return

    def closeEvent(self, event):
        self.completeLogger.info('Project Finished. View Details.')
        return

    def exit_window(self):
        # NOTE: executes the closeEvent function.
        self.close()
        return

    def view_log_action(self):
        self.logWindow.show()
        self.logWindow.activateWindow()
        self.close()
        return

if __name__ == '__main__':
    # Add support for multiprocessing for when code has been frozen as Windows executable.
    # https://docs.python.org/2/library/multiprocessing.html#miscellaneous
    # freeze_support()

    # Enable dpi scaling so if the windows system setting for scale is large than 100%
    # the application window scales accordingly without getting distorted.
    QApplication.setAttribute(Qt.AA_EnableHighDpiScaling, on=True)

    # Start the application.
    print('\nApplication started.')
    app = QApplication(sys.argv)

    # Set the main window style. Use windowsvista for newest windows style.
    # print(QStyleFactory.keys())
    # app.setStyle(QStyleFactory.create('windowsvista'))
    # app.setStyle(QStyleFactory.create('Windows'))
    app.setStyle(QStyleFactory.create('Fusion'))

    # Initialize and show the main window.
    mainGui = CompleteWindow()
    mainGui.show()
    app.exit(app.exec_())
    print('\nApplication closed.')
