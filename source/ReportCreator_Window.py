# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'ReportCreator_Window.ui'
##
## Created by: Qt User Interface Compiler version 5.15.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(1080, 720)
        sizePolicy = QSizePolicy(QSizePolicy.Ignored, QSizePolicy.Ignored)
        sizePolicy.setHorizontalStretch(100)
        sizePolicy.setVerticalStretch(100)
        sizePolicy.setHeightForWidth(MainWindow.sizePolicy().hasHeightForWidth())
        MainWindow.setSizePolicy(sizePolicy)
        MainWindow.setMinimumSize(QSize(1080, 720))
        self.actionFileExit = QAction(MainWindow)
        self.actionFileExit.setObjectName(u"actionFileExit")
        font = QFont()
        font.setPointSize(12)
        self.actionFileExit.setFont(font)
        self.actionEditDefaults = QAction(MainWindow)
        self.actionEditDefaults.setObjectName(u"actionEditDefaults")
        self.actionEditDefaults.setEnabled(True)
        self.actionEditDefaults.setFont(font)
        self.actionSelectApplication = QAction(MainWindow)
        self.actionSelectApplication.setObjectName(u"actionSelectApplication")
        self.actionEditPowerpointSettings = QAction(MainWindow)
        self.actionEditPowerpointSettings.setObjectName(u"actionEditPowerpointSettings")
        self.actionDeviationSettings = QAction(MainWindow)
        self.actionDeviationSettings.setObjectName(u"actionDeviationSettings")
        self.actionDeviationSettings.setFont(font)
        self.actionNew = QAction(MainWindow)
        self.actionNew.setObjectName(u"actionNew")
        self.actionNew.setFont(font)
        self.actionDocumentation = QAction(MainWindow)
        self.actionDocumentation.setObjectName(u"actionDocumentation")
        self.actionDocumentation.setFont(font)
        self.actionAbout = QAction(MainWindow)
        self.actionAbout.setObjectName(u"actionAbout")
        self.actionAbout.setFont(font)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.centralwidget.setFont(font)
        self.verticalLayout = QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.groupBoxDirectoryInput = QGroupBox(self.centralwidget)
        self.groupBoxDirectoryInput.setObjectName(u"groupBoxDirectoryInput")
        sizePolicy1 = QSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Fixed)
        sizePolicy1.setHorizontalStretch(100)
        sizePolicy1.setVerticalStretch(30)
        sizePolicy1.setHeightForWidth(self.groupBoxDirectoryInput.sizePolicy().hasHeightForWidth())
        self.groupBoxDirectoryInput.setSizePolicy(sizePolicy1)
        font1 = QFont()
        font1.setPointSize(12)
        font1.setBold(True)
        font1.setWeight(75)
        self.groupBoxDirectoryInput.setFont(font1)
        self.gridLayout_2 = QGridLayout(self.groupBoxDirectoryInput)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.pushButtonOpenDir = QPushButton(self.groupBoxDirectoryInput)
        self.pushButtonOpenDir.setObjectName(u"pushButtonOpenDir")
        sizePolicy2 = QSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Maximum)
        sizePolicy2.setHorizontalStretch(100)
        sizePolicy2.setVerticalStretch(100)
        sizePolicy2.setHeightForWidth(self.pushButtonOpenDir.sizePolicy().hasHeightForWidth())
        self.pushButtonOpenDir.setSizePolicy(sizePolicy2)
        self.pushButtonOpenDir.setMinimumSize(QSize(0, 70))
        font2 = QFont()
        font2.setPointSize(12)
        font2.setBold(False)
        font2.setWeight(50)
        self.pushButtonOpenDir.setFont(font2)

        self.gridLayout_2.addWidget(self.pushButtonOpenDir, 0, 1, 1, 1)

        self.pushButtonAddDir = QPushButton(self.groupBoxDirectoryInput)
        self.pushButtonAddDir.setObjectName(u"pushButtonAddDir")
        sizePolicy2.setHeightForWidth(self.pushButtonAddDir.sizePolicy().hasHeightForWidth())
        self.pushButtonAddDir.setSizePolicy(sizePolicy2)
        self.pushButtonAddDir.setMinimumSize(QSize(0, 70))
        self.pushButtonAddDir.setFont(font2)

        self.gridLayout_2.addWidget(self.pushButtonAddDir, 0, 0, 1, 1)

        self.pushButtonRemoveDir = QPushButton(self.groupBoxDirectoryInput)
        self.pushButtonRemoveDir.setObjectName(u"pushButtonRemoveDir")
        sizePolicy2.setHeightForWidth(self.pushButtonRemoveDir.sizePolicy().hasHeightForWidth())
        self.pushButtonRemoveDir.setSizePolicy(sizePolicy2)
        self.pushButtonRemoveDir.setMinimumSize(QSize(0, 70))
        self.pushButtonRemoveDir.setFont(font2)

        self.gridLayout_2.addWidget(self.pushButtonRemoveDir, 0, 3, 1, 1)


        self.verticalLayout.addWidget(self.groupBoxDirectoryInput)

        self.treeWidget = QTreeWidget(self.centralwidget)
        __qtreewidgetitem = QTreeWidgetItem(self.treeWidget)
        QTreeWidgetItem(__qtreewidgetitem)
        QTreeWidgetItem(__qtreewidgetitem)
        __qtreewidgetitem1 = QTreeWidgetItem(self.treeWidget)
        QTreeWidgetItem(__qtreewidgetitem1)
        self.treeWidget.setObjectName(u"treeWidget")
        sizePolicy3 = QSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.MinimumExpanding)
        sizePolicy3.setHorizontalStretch(100)
        sizePolicy3.setVerticalStretch(100)
        sizePolicy3.setHeightForWidth(self.treeWidget.sizePolicy().hasHeightForWidth())
        self.treeWidget.setSizePolicy(sizePolicy3)

        self.verticalLayout.addWidget(self.treeWidget)

        self.groupBoxProcessingStatus = QGroupBox(self.centralwidget)
        self.groupBoxProcessingStatus.setObjectName(u"groupBoxProcessingStatus")
        sizePolicy4 = QSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Fixed)
        sizePolicy4.setHorizontalStretch(100)
        sizePolicy4.setVerticalStretch(50)
        sizePolicy4.setHeightForWidth(self.groupBoxProcessingStatus.sizePolicy().hasHeightForWidth())
        self.groupBoxProcessingStatus.setSizePolicy(sizePolicy4)
        self.groupBoxProcessingStatus.setMinimumSize(QSize(0, 178))
        self.groupBoxProcessingStatus.setFont(font)
        self.gridLayout = QGridLayout(self.groupBoxProcessingStatus)
        self.gridLayout.setObjectName(u"gridLayout")
        self.comboBoxReportUnits = QComboBox(self.groupBoxProcessingStatus)
        self.comboBoxReportUnits.addItem("")
        self.comboBoxReportUnits.addItem("")
        self.comboBoxReportUnits.setObjectName(u"comboBoxReportUnits")
        sizePolicy5 = QSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Fixed)
        sizePolicy5.setHorizontalStretch(100)
        sizePolicy5.setVerticalStretch(100)
        sizePolicy5.setHeightForWidth(self.comboBoxReportUnits.sizePolicy().hasHeightForWidth())
        self.comboBoxReportUnits.setSizePolicy(sizePolicy5)
        self.comboBoxReportUnits.setMinimumSize(QSize(0, 45))
        self.comboBoxReportUnits.setFont(font)

        self.gridLayout.addWidget(self.comboBoxReportUnits, 0, 0, 1, 2)

        self.pushButtonResetProcessing = QPushButton(self.groupBoxProcessingStatus)
        self.pushButtonResetProcessing.setObjectName(u"pushButtonResetProcessing")
        sizePolicy5.setHeightForWidth(self.pushButtonResetProcessing.sizePolicy().hasHeightForWidth())
        self.pushButtonResetProcessing.setSizePolicy(sizePolicy5)
        self.pushButtonResetProcessing.setMinimumSize(QSize(0, 45))
        self.pushButtonResetProcessing.setFont(font)

        self.gridLayout.addWidget(self.pushButtonResetProcessing, 0, 2, 1, 1)

        self.pushButtonStartProcessing = QPushButton(self.groupBoxProcessingStatus)
        self.pushButtonStartProcessing.setObjectName(u"pushButtonStartProcessing")
        sizePolicy5.setHeightForWidth(self.pushButtonStartProcessing.sizePolicy().hasHeightForWidth())
        self.pushButtonStartProcessing.setSizePolicy(sizePolicy5)
        self.pushButtonStartProcessing.setMinimumSize(QSize(0, 45))
        self.pushButtonStartProcessing.setFont(font)

        self.gridLayout.addWidget(self.pushButtonStartProcessing, 0, 3, 1, 1)

        self.pushButtonCancelProcessing = QPushButton(self.groupBoxProcessingStatus)
        self.pushButtonCancelProcessing.setObjectName(u"pushButtonCancelProcessing")
        self.pushButtonCancelProcessing.setEnabled(False)
        sizePolicy5.setHeightForWidth(self.pushButtonCancelProcessing.sizePolicy().hasHeightForWidth())
        self.pushButtonCancelProcessing.setSizePolicy(sizePolicy5)
        self.pushButtonCancelProcessing.setMinimumSize(QSize(0, 45))
        self.pushButtonCancelProcessing.setFont(font)
        self.pushButtonCancelProcessing.setAutoDefault(False)
        self.pushButtonCancelProcessing.setFlat(False)

        self.gridLayout.addWidget(self.pushButtonCancelProcessing, 0, 4, 1, 1)

        self.pushButtonViewLog = QPushButton(self.groupBoxProcessingStatus)
        self.pushButtonViewLog.setObjectName(u"pushButtonViewLog")
        self.pushButtonViewLog.setEnabled(True)
        sizePolicy5.setHeightForWidth(self.pushButtonViewLog.sizePolicy().hasHeightForWidth())
        self.pushButtonViewLog.setSizePolicy(sizePolicy5)
        self.pushButtonViewLog.setMinimumSize(QSize(0, 45))
        self.pushButtonViewLog.setFont(font)
        self.pushButtonViewLog.setAutoDefault(False)
        self.pushButtonViewLog.setFlat(False)

        self.gridLayout.addWidget(self.pushButtonViewLog, 0, 5, 1, 1)

        self.labelStatusCurrentFile = QLabel(self.groupBoxProcessingStatus)
        self.labelStatusCurrentFile.setObjectName(u"labelStatusCurrentFile")
        sizePolicy6 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy6.setHorizontalStretch(100)
        sizePolicy6.setVerticalStretch(100)
        sizePolicy6.setHeightForWidth(self.labelStatusCurrentFile.sizePolicy().hasHeightForWidth())
        self.labelStatusCurrentFile.setSizePolicy(sizePolicy6)
        self.labelStatusCurrentFile.setMinimumSize(QSize(169, 42))
        self.labelStatusCurrentFile.setFont(font)

        self.gridLayout.addWidget(self.labelStatusCurrentFile, 1, 0, 1, 1)

        self.labelStatusCurrentFilename = QLabel(self.groupBoxProcessingStatus)
        self.labelStatusCurrentFilename.setObjectName(u"labelStatusCurrentFilename")
        sizePolicy5.setHeightForWidth(self.labelStatusCurrentFilename.sizePolicy().hasHeightForWidth())
        self.labelStatusCurrentFilename.setSizePolicy(sizePolicy5)
        self.labelStatusCurrentFilename.setMinimumSize(QSize(867, 42))
        self.labelStatusCurrentFilename.setFont(font)

        self.gridLayout.addWidget(self.labelStatusCurrentFilename, 1, 1, 1, 5)

        self.progressBar = QProgressBar(self.groupBoxProcessingStatus)
        self.progressBar.setObjectName(u"progressBar")
        sizePolicy5.setHeightForWidth(self.progressBar.sizePolicy().hasHeightForWidth())
        self.progressBar.setSizePolicy(sizePolicy5)
        self.progressBar.setMinimumSize(QSize(1042, 41))
        self.progressBar.setFont(font)
        self.progressBar.setStyleSheet(u"")
        self.progressBar.setValue(10)

        self.gridLayout.addWidget(self.progressBar, 2, 0, 1, 6)


        self.verticalLayout.addWidget(self.groupBoxProcessingStatus)

        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(MainWindow)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 1080, 27))
        self.menubar.setFont(font)
        self.menuFile = QMenu(self.menubar)
        self.menuFile.setObjectName(u"menuFile")
        self.menuFile.setFont(font)
        self.menuHelp = QMenu(self.menubar)
        self.menuHelp.setObjectName(u"menuHelp")
        self.menuSettings = QMenu(self.menubar)
        self.menuSettings.setObjectName(u"menuSettings")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QStatusBar(MainWindow)
        self.statusbar.setObjectName(u"statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.menubar.addAction(self.menuFile.menuAction())
        self.menubar.addAction(self.menuSettings.menuAction())
        self.menubar.addAction(self.menuHelp.menuAction())
        self.menuFile.addAction(self.actionNew)
        self.menuFile.addSeparator()
        self.menuFile.addAction(self.actionFileExit)
        self.menuFile.addSeparator()
        self.menuHelp.addAction(self.actionDocumentation)
        self.menuHelp.addSeparator()
        self.menuHelp.addAction(self.actionAbout)
        self.menuSettings.addAction(self.actionEditDefaults)
        self.menuSettings.addSeparator()
        self.menuSettings.addAction(self.actionDeviationSettings)

        self.retranslateUi(MainWindow)

        self.pushButtonCancelProcessing.setDefault(False)
        self.pushButtonViewLog.setDefault(False)


        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"MainWindow", None))
        self.actionFileExit.setText(QCoreApplication.translate("MainWindow", u"Exit", None))
        self.actionEditDefaults.setText(QCoreApplication.translate("MainWindow", u"Default Templates/Images", None))
        self.actionSelectApplication.setText(QCoreApplication.translate("MainWindow", u"Select Application", None))
        self.actionEditPowerpointSettings.setText(QCoreApplication.translate("MainWindow", u"Edit Report Settings", None))
        self.actionDeviationSettings.setText(QCoreApplication.translate("MainWindow", u"Deviation Report Settings", None))
        self.actionNew.setText(QCoreApplication.translate("MainWindow", u"New", None))
        self.actionDocumentation.setText(QCoreApplication.translate("MainWindow", u"Documentation", None))
        self.actionAbout.setText(QCoreApplication.translate("MainWindow", u"About", None))
        self.groupBoxDirectoryInput.setTitle(QCoreApplication.translate("MainWindow", u"Select Inputs:", None))
#if QT_CONFIG(tooltip)
        self.pushButtonOpenDir.setToolTip(QCoreApplication.translate("MainWindow", u"<html><head/><body><p>Open the selected directory in a windows explorer.</p></body></html>", None))
#endif // QT_CONFIG(tooltip)
        self.pushButtonOpenDir.setText(QCoreApplication.translate("MainWindow", u"Open Selected Tree Directory", None))
#if QT_CONFIG(tooltip)
        self.pushButtonAddDir.setToolTip(QCoreApplication.translate("MainWindow", u"<html><head/><body><p>Add project directory to processing report queue.</p></body></html>", None))
#endif // QT_CONFIG(tooltip)
        self.pushButtonAddDir.setText(QCoreApplication.translate("MainWindow", u"Add Project Directory", None))
#if QT_CONFIG(tooltip)
        self.pushButtonRemoveDir.setToolTip(QCoreApplication.translate("MainWindow", u"<html><head/><body><p>Remove the selected directory from the processing report queue.</p></body></html>", None))
#endif // QT_CONFIG(tooltip)
        self.pushButtonRemoveDir.setText(QCoreApplication.translate("MainWindow", u"Remove Selected Tree Directory", None))
        ___qtreewidgetitem = self.treeWidget.headerItem()
        ___qtreewidgetitem.setText(4, QCoreApplication.translate("MainWindow", u"Status", None));
        ___qtreewidgetitem.setText(3, QCoreApplication.translate("MainWindow", u"Header Info", None));
        ___qtreewidgetitem.setText(2, QCoreApplication.translate("MainWindow", u"Content Mapper", None));
        ___qtreewidgetitem.setText(1, QCoreApplication.translate("MainWindow", u"PPT Template", None));
        ___qtreewidgetitem.setText(0, QCoreApplication.translate("MainWindow", u"Dir Added", None));

        __sortingEnabled = self.treeWidget.isSortingEnabled()
        self.treeWidget.setSortingEnabled(False)
        ___qtreewidgetitem1 = self.treeWidget.topLevelItem(0)
        ___qtreewidgetitem1.setText(3, QCoreApplication.translate("MainWindow", u"-", None));
        ___qtreewidgetitem1.setText(2, QCoreApplication.translate("MainWindow", u"-", None));
        ___qtreewidgetitem1.setText(1, QCoreApplication.translate("MainWindow", u"-", None));
        ___qtreewidgetitem1.setText(0, QCoreApplication.translate("MainWindow", u"QB#1", None));
        ___qtreewidgetitem2 = ___qtreewidgetitem1.child(0)
        ___qtreewidgetitem2.setText(0, QCoreApplication.translate("MainWindow", u"Sample1", None));
        ___qtreewidgetitem3 = ___qtreewidgetitem1.child(1)
        ___qtreewidgetitem3.setText(0, QCoreApplication.translate("MainWindow", u"Sample2", None));
        ___qtreewidgetitem4 = self.treeWidget.topLevelItem(1)
        ___qtreewidgetitem4.setText(0, QCoreApplication.translate("MainWindow", u"QB#2", None));
        ___qtreewidgetitem5 = ___qtreewidgetitem4.child(0)
        ___qtreewidgetitem5.setText(0, QCoreApplication.translate("MainWindow", u"Sample1", None));
        self.treeWidget.setSortingEnabled(__sortingEnabled)

        self.groupBoxProcessingStatus.setTitle(QCoreApplication.translate("MainWindow", u"Processing:", None))
        self.comboBoxReportUnits.setItemText(0, QCoreApplication.translate("MainWindow", u"Millimeters", None))
        self.comboBoxReportUnits.setItemText(1, QCoreApplication.translate("MainWindow", u"Inches", None))

#if QT_CONFIG(tooltip)
        self.comboBoxReportUnits.setToolTip(QCoreApplication.translate("MainWindow", u"<html><head/><body><p>Select units to be used in report.</p></body></html>", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(tooltip)
        self.pushButtonResetProcessing.setToolTip(QCoreApplication.translate("MainWindow", u"<html><head/><body><p>Reset the application to setup a new processing queue.</p></body></html>", None))
#endif // QT_CONFIG(tooltip)
        self.pushButtonResetProcessing.setText(QCoreApplication.translate("MainWindow", u"Reset", None))
#if QT_CONFIG(tooltip)
        self.pushButtonStartProcessing.setToolTip(QCoreApplication.translate("MainWindow", u"<html><head/><body><p>Start the processing report queue.</p></body></html>", None))
#endif // QT_CONFIG(tooltip)
        self.pushButtonStartProcessing.setText(QCoreApplication.translate("MainWindow", u"Start", None))
#if QT_CONFIG(tooltip)
        self.pushButtonCancelProcessing.setToolTip(QCoreApplication.translate("MainWindow", u"<html><head/><body><p>Cancel the current processing report queue.</p><p><br/></p></body></html>", None))
#endif // QT_CONFIG(tooltip)
        self.pushButtonCancelProcessing.setText(QCoreApplication.translate("MainWindow", u"Cancel", None))
#if QT_CONFIG(tooltip)
        self.pushButtonViewLog.setToolTip(QCoreApplication.translate("MainWindow", u"<html><head/><body><p>View logged information pertaining to each report being created for project directories in the processing queue.</p></body></html>", None))
#endif // QT_CONFIG(tooltip)
        self.pushButtonViewLog.setText(QCoreApplication.translate("MainWindow", u"View Log", None))
        self.labelStatusCurrentFile.setText(QCoreApplication.translate("MainWindow", u"Current File:", None))
#if QT_CONFIG(tooltip)
        self.labelStatusCurrentFilename.setToolTip(QCoreApplication.translate("MainWindow", u"<html><head/><body><p>Current project directory being processed.</p></body></html>", None))
#endif // QT_CONFIG(tooltip)
        self.labelStatusCurrentFilename.setText(QCoreApplication.translate("MainWindow", u"None", None))
        self.menuFile.setTitle(QCoreApplication.translate("MainWindow", u"File", None))
        self.menuHelp.setTitle(QCoreApplication.translate("MainWindow", u"Help", None))
        self.menuSettings.setTitle(QCoreApplication.translate("MainWindow", u"Settings", None))
    # retranslateUi

