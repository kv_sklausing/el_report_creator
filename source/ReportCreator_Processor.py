'''
Processor for ReportCreator
'''

import os
import pickle
import numpy
import pandas
import copy
import math
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('Agg')

from matplotlib.ticker import (MultipleLocator, FormatStrFormatter)
import traceback

import KV_Imaging as kvimg


import pptx
import pptx.shapes.placeholder
from pptx.util import Inches
import win32com.client

IMAGE_EXTENSIONS = ['.jpg', '.jpeg', '.png', '.tiff', '.tif']
EXCEL_EXTENSIONS = ['.xlsx', '.xls', '.xlsm']
CSV_EXTENSIONS = ['.csv']

class ReportCreator:
    """
    Class that handles the primary logic flow for the Report Creator.
    """

    def __init__(self, signalAnalysisThreadLog, signalAnalysisFileStatus):

        self.signalAnalysisThreadLog = signalAnalysisThreadLog
        self.signalAnalysisFileStatus = signalAnalysisFileStatus

        # Initialize Project Paths
        self.projectDirectory = None
        self.initialSheetsDirectory = None
        self.sampleDirPaths = None

        # Initial Project Files
        self.contentMapperFile = None
        self.headerInfoFile = None

        # Initialize PPT variables.
        self.pptTemplate = None

        # Initialize Bin Variables
        self.varianceExpImgFile = None
        self.noImgFoundFile = None

        # Initialize Variables class uses later
        self.mapperDict = None
        self.mapperDictUpdated = dict()
        self.mapperHeaders = None
        self.headerInfoImg = None
        self.outputPPTXReport = None
        self.cmapObj = None
        self.normObj = None

        self.projectUnits = None
        if self.projectUnits == 0:
            self.reportUnits = 'mm'
        else:
            self.reportUnits = 'in'


    def read_excel_mapper(self):
        # print('Reading excel mapper.')
        self.signalAnalysisThreadLog.emit('Reading excel mapper.', 'info')


        mapperDf = pandas.read_excel(self.contentMapperFile, dtype=str)
        slideNumDict = mapperDf.transpose().to_dict('series')
        self.mapperHeaders = mapperDf.columns.values
        keyIdxs = numpy.where(numpy.char.find(self.mapperHeaders.astype(str), 'Key') == 0)[0]
        valueIdx = keyIdxs + 1

        VALID_TRUE = 'true'
        self.mapperDict = dict()
        for slideNum, slideSeries in slideNumDict.items():
            slideSeries.fillna('EXCLUDE', inplace=True)
            sampleRepeat = slideSeries['Sample Repeat'].lower().replace(" ", "") == VALID_TRUE
            # sampleRepeat = bool(slideSeries['Sample Repeat'])
            slideNumKey = 'Slide %s' % slideNum

            # Log Slide found
            # print('Slide Found: %s' % slideSeries['Slide Layout Name Template'])
            self.signalAnalysisThreadLog.emit('Slide Found in Content Mapper:   %s' % slideSeries['Slide Layout Name Template'], 'info')

            self.mapperDict[slideNumKey] = {'Slide Name': slideSeries['Slide Layout Name Template'],
                                            'Sample Repeat': sampleRepeat,
                                            'Source Directory': slideSeries['Source']}
            for mapPair, keyIdx in enumerate(keyIdxs):
                mapName = 'Map %s' % mapPair
                mapSeries = slideSeries.iloc[keyIdx:keyIdx + 2]
                mapSeries = mapSeries.rename({mapSeries.index[0]: 'Key', mapSeries.index[1]: 'Value'})
                if mapSeries['Value'] == 'EXCLUDE':
                    continue
                fileType = 'Text'
                fileExt = 'N/A'
                checkExtImgFile = any(imgExt.lower() in str(mapSeries['Value']) for imgExt in IMAGE_EXTENSIONS)
                if checkExtImgFile:
                    fileType = 'Image'
                    fileExt = IMAGE_EXTENSIONS
                checkExtXcelFile = any(imgExt.lower() in str(mapSeries['Value']) for imgExt in EXCEL_EXTENSIONS)
                if checkExtXcelFile:
                    fileType = 'Excel'
                    fileExt = EXCEL_EXTENSIONS
                checkExtCsvFile = any(imgExt.lower() in str(mapSeries['Value']) for imgExt in CSV_EXTENSIONS)
                if checkExtCsvFile:
                    fileType = 'CSV'
                    fileExt = CSV_EXTENSIONS

                mapPairDict = mapSeries.to_dict()
                mapPairDict['File Type'] = fileType
                mapPairDict['File Ext'] = fileExt
                self.mapperDict[slideNumKey][mapName] = mapPairDict

        self.signalAnalysisThreadLog.emit('Item Mapping Complete!', 'info')
        return


    def read_header_info(self):

        if os.path.isfile(self.headerInfoFile):
            self.signalAnalysisThreadLog.emit('Reading header info file:  {fullfile}.'.format(fullfile=self.headerInfoFile), 'info')
            headInfoDf = pandas.read_excel(self.headerInfoFile, index_col=None, header=2, names=['Index1', 'Index2', 'Value'])
            headInfoDf.dropna(how='all', subset=['Index1', 'Index2'], inplace=True, axis=0)

            headInfoDfTable = headInfoDf.fillna('')

            headerFig, headerAx = plt.subplots(figsize=(10, 6.5))
            figure, axis, mpl_table = self.render_header_table(headInfoDfTable, fig=headerFig, ax=headerAx, autoSizeFont=True)

            self.headerInfoImg = '%s.png' % os.path.splitext(self.headerInfoFile)[0]
            figure.savefig(self.headerInfoImg, bbox_inches='tight')

            self.signalAnalysisThreadLog.emit('\t --- Saved Header Table: %s' % os.path.basename(self.headerInfoImg), 'info')
        else:
            # print('No Header Info.')
            self.signalAnalysisThreadLog.emit('\t --- Invalid Header Info File: %s' % self.headerInfoFile, 'info')
            self.headerInfoImg = self.noImgFoundFile

        return


    def render_header_table(self, data, fig, ax, autoSizeFont=False, **kwargs):

        self.signalAnalysisThreadLog.emit('Rendering header info table.', 'info')

        ax.axis('off')
        mpl_table = ax.table(cellText=data.values, bbox=(0, 0, 1, 1), **kwargs)

        for cellIdx, cellObj in mpl_table._cells.items():
            cellRow = cellIdx[0]
            cellCol = cellIdx[1]
            cellText = cellObj._text.get_text()
            if cellCol == 0 and cellText == '':
                cellObj.set_edgecolor('gray')

        for cellIdx, cellObj in mpl_table._cells.items():
            cellRow = cellIdx[0]
            cellCol = cellIdx[1]
            if cellCol == 0 and cellRow == 0:
                cellObj.set_edgecolor('white')

        for cellIdx, cellObj in mpl_table._cells.items():
            cellRow = cellIdx[0]
            cellCol = cellIdx[1]
            cellText = cellObj._text.get_text()
            textColor = 'black'
            if (cellCol == 0 or cellCol == 1) and cellText != '':
                cellObj.set_edgecolor('black')
                cellObj.set_facecolor('white')
                if '*' in cellText:
                    textColor = 'red'
                if '**' in cellText:
                    textColor = 'blue'
                cellObj.set_text_props(weight='bold', color=textColor)
                cellObj._loc = 'left'

            # if cellCol == 2:
            #     cellObj.set_edgecolor('black')
            #     cellObj.set_facecolor('#FFFF99')
            #     cellObj.set_text_props(weight='bold', color='black')
            #     cellObj._loc = 'right'

        # mpl_table.auto_set_font_size(True)
        mpl_table.set_fontsize(10)
        mpl_table.auto_set_font_size(autoSizeFont)


        # self.signalAnalysisThreadLog.emit('Rendering header info table complete.', 'info')
        return fig, ax, mpl_table


    def update_content_path(self):
        self.signalAnalysisThreadLog.emit('Updating sample repeat content.', 'info')
        removeSlideNums = list()
        for slideNum, slideInfo in self.mapperDict.items():
            sampleRepeat = slideInfo['Sample Repeat']
            sourceDir = slideInfo['Source Directory']
            if sampleRepeat:
                sampleDirs = kvimg.valid_dirs_in_directory(sourceDir)
                for sampleDir in sampleDirs:
                    sampleName = os.path.basename(sampleDir)
                    cavityNum = sampleName
                    if 'cav-' in sampleName.lower():
                        cavityNum = sampleName.lower().split('cav-')[-1]
                    if 'cav_' in sampleName.lower():
                        cavityNum = sampleName.lower().split('cav_')[-1]
                    if 'cavity-' in sampleName.lower():
                        cavityNum = sampleName.lower().split('cavity-')[-1]
                    if 'cavity_' in sampleName.lower():
                        cavityNum = sampleName.lower().split('cavity_')[-1]
                    slideInfoRepeat = copy.deepcopy(slideInfo)
                    slideInfoRepeat['Source Directory'] = sampleDir
                    slideInfoRepeat['Cavity Number'] = {'Key': 'Mold Cavity PH',
                                                        'Value': 'Cav-%s' % str(cavityNum),
                                                        'File Type': 'Text',
                                                        'File Ext': 'N/A'}
                    slideInfoRepeat['Sample Name'] = sampleName
                    self.mapperDictUpdated['%s_%s' % (slideNum, sampleName)] = slideInfoRepeat
                removeSlideNums.append(slideNum)
            else:
                self.mapperDictUpdated[slideNum] = slideInfo

        def check_file_exists(fileToCheck, inputSourceDir, fileExtList, fileType):
            if os.path.isfile(fileToCheck):
                return fileToCheck, fileType, True
            else:
                fileToCheckName = os.path.splitext(fileToCheck)[0]
                fileList = kvimg.get_files(inputSourceDir, fileExtList, contains=fileToCheckName)
                if fileList:
                    fullFile = fileList[0]
                    return fullFile, fileType, True
                else:
                    self.signalAnalysisThreadLog.emit("File not found:  {fullFile}.{extensions}".format(
                        fullFile=os.path.join(inputSourceDir, fileToCheckName),
                        extensions=str(fileExtList)),
                        'warn')
                    return self.noImgFoundFile, 'Image', False

        removeOldSlideList = list()
        newSlideList = list()
        for slideNumUpdated, slideInfoUpdated in self.mapperDictUpdated.items():
            sampleRepeatUpdated = slideInfoUpdated['Sample Repeat']
            sourceDirUpdated = slideInfoUpdated['Source Directory']
            try:
                cavityNum = slideInfoUpdated['Cavity Number']['Value']
            except:
                cavityNum = None
            for key, keyVal in slideInfoUpdated.items():
                if type(keyVal) == dict and keyVal['File Type'] != 'Text':
                    projectInfoExcel = False
                    fullPath, fileTypeUpdate, found = check_file_exists(keyVal['Value'], sourceDirUpdated,
                                                                 keyVal['File Ext'], keyVal['File Type'])
                    keyVal['Value'] = fullPath
                    keyVal['File Type'] = fileTypeUpdate
                    if keyVal['Key'] == 'Header Data':
                        if fileTypeUpdate is None:
                            self.headerInfoFile = fullPath
                            self.read_header_info()
                            keyVal['Value'] = self.headerInfoImg
                        else:
                            fullPath, fileTypeUpdate, found = check_file_exists(self.headerInfoFile, sourceDirUpdated,
                                                                         keyVal['File Ext'], keyVal['File Type'])
                            if found:
                                self.headerInfoFile = fullPath
                                self.read_header_info()
                                keyVal['Value'] = self.headerInfoImg
                            else:
                                keyVal['Value'] = fullPath
                if type(keyVal) == dict and keyVal['File Type'] == 'CSV':
                    try:
                        csvImg = self.read_csv_file(keyVal['Value'], keyVal['Key'], cavityNum)
                        for csvImgIdx, csvImgItem in enumerate(csvImg):
                            slideInfoUpdatedNew = copy.deepcopy(slideInfoUpdated)
                            keyVal = slideInfoUpdatedNew[key]
                            keyVal['Value'] = csvImgItem
                            newSlideName = '%s_Page-%i' % (slideNumUpdated, csvImgIdx)
                            # self.mapperDictUpdated.update({newSlideName: slideInfoUpdatedNew})
                            newSlideList.append({newSlideName: slideInfoUpdatedNew})
                            removeOldSlideList.append(slideNumUpdated)
                    except:
                        errorMessage = traceback.format_exc()
                        self.signalAnalysisThreadLog.emit(errorMessage, 'error')
                        removeOldSlideList.append(slideNumUpdated)
                if type(keyVal) == dict and keyVal['File Type'] == 'Excel':
                    try:
                        excelImg = self.read_excel_file(keyVal['Value'], keyVal['Key'], cavityNum)
                        for excelImgIdx, excelImgItem in enumerate(excelImg):
                            slideInfoUpdatedNew = copy.deepcopy(slideInfoUpdated)
                            keyVal = slideInfoUpdatedNew[key]
                            keyVal['Value'] = excelImgItem
                            newSlideName = '%s_Page-%i' % (slideNumUpdated, excelImgIdx)
                            newSlideList.append({newSlideName: slideInfoUpdatedNew})
                            removeOldSlideList.append(slideNumUpdated)
                    except:
                        errorMessage = traceback.format_exc()
                        self.signalAnalysisThreadLog.emit(errorMessage, 'error')
                        removeOldSlideList.append(slideNumUpdated)

        for newSlideDict in newSlideList:
            self.mapperDictUpdated.update(newSlideDict)

        for removeOldSlide in sorted(set(removeOldSlideList)):
            del self.mapperDictUpdated[removeOldSlide]


        reorderDf = pandas.DataFrame()
        for slideKeyIdx, slideKey in enumerate(self.mapperDictUpdated.keys()):
            slideNum = slideKey.split('Slide ')[-1].split('_')[0]
            reorderDf.loc[slideKeyIdx, 'Slide Num'] = int(slideNum)
            reorderDf.loc[slideKeyIdx, 'Slide Name'] = slideKey
        reorderDf.sort_values(by=['Slide Num', 'Slide Name'], inplace=True)
        orderedKeys = reorderDf.loc[:, 'Slide Name'].reset_index(drop=True)

        newMainDict = dict()
        for slideKeyLbl in orderedKeys.values:
            newMainDict.update({slideKeyLbl: self.mapperDictUpdated[slideKeyLbl]})

        self.mapperDictUpdated = newMainDict

        self.signalAnalysisThreadLog.emit('Updating sample repeat content complete.', 'info')

        return


    def render_deviation_plot(self, data, fig, ax, **kwargs):

        self.signalAnalysisThreadLog.emit('\t --- Rendering deviation plot.', 'info')
        self.maxCritical = 0.05
        orderOfMagDict = {1: 1,
                          2: 10,
                          3: 100,
                          4: 1000}

        data.iloc[:, 0] = pandas.to_numeric(data.iloc[:, 0], errors='coerce')
        data.dropna(inplace=True)
        xData = data.iloc[:, 0]
        yData = data.iloc[:, 1]

        colorMapper = self.cmapObj(self.normObj(xData.values))

        # totalXRange = maxNominal - minNominal
        # numXTicks = (totalXRange / maxCritical) + 1
        # xTicks = numpy.linspace(minNominal, maxNominal, int(numXTicks))

        maxY = math.ceil(max(yData))
        maxYString = str(maxY)
        ordOfMagId = len(maxYString)
        orderOfMag = orderOfMagDict[ordOfMagId]
        # maxYTick = math.ceil(maxY / orderOfMag) * orderOfMag
        # numYticks = (maxYTick / orderOfMag) + 1
        # yTicks = numpy.linspace(0, maxYTick, int(numYticks))

        faceColorHex = '#343434'
        gridLineColorHex = '#656565'
        tickParaColorHex = '#C1C1C1'

        fig.set_facecolor(faceColorHex)
        ax.set_facecolor(faceColorHex)

        ax.xaxis.set_major_locator(MultipleLocator(self.maxCritical))
        ax.xaxis.set_major_formatter(FormatStrFormatter('%0.2f'))
        ax.xaxis.set_minor_locator(MultipleLocator(self.maxCritical / 2))
        # ax.set_xticks(xTicks)
        # ax.set_yticks(yTicks)
        ax.yaxis.set_major_locator(MultipleLocator(orderOfMag))
        ax.yaxis.set_major_formatter(FormatStrFormatter('%d'))
        ax.yaxis.set_minor_locator(MultipleLocator(orderOfMag / 5))
        ax.set_axisbelow(True)
        ax.tick_params(colors=tickParaColorHex, which='major', size=2)
        ax.grid(True, which='major', color=gridLineColorHex, linestyle='--', linewidth=2)
        ax.tick_params(colors=tickParaColorHex, which='minor', size=1)
        ax.grid(True, which='minor', color=gridLineColorHex, linestyle='--', linewidth=1)
        for spineKey, spine in ax.spines.items():
            spine.set_color(tickParaColorHex)

        ax.bar(xData, yData, color=colorMapper, width=xData.diff().min(), align='edge')

        ax.set_title('Deviation Distribution', color=tickParaColorHex)
        ax.set_xlabel('Deviation [%s]' % self.reportUnits, color=tickParaColorHex)
        ax.set_ylabel('Surface [$\mathregular{%s^2}$]' % self.reportUnits, color=tickParaColorHex)

        # self.signalAnalysisThreadLog.emit('Rendering deviation plot complete.', 'info')
        return fig, ax


    def format_results_data(self, resultsFile, type='csv'):

        self.signalAnalysisThreadLog.emit('\t --- Formatting results table.', 'info')
        outputFormattedFile = os.path.join(os.path.dirname(resultsFile),
                                           '%s_formatted.xlsx' % os.path.splitext(os.path.basename(resultsFile))[0])

        if type == 'csv':
            fullResultsDf = pandas.read_csv(resultsFile)
        elif type == 'excel':
            fullResultsDf = pandas.read_excel(resultsFile)
        else:
            raise ValueError('Not excel nor csv file type.')

        # fullResultsDf.dropna(axis=1, inplace=True, how='all')
        columnValues = pandas.Series(fullResultsDf.keys().to_list())

        unitsLen = 'N/A'
        unitsAng = 'N/A'
        actualValHeader = ''
        nomValHeader = ''
        tolHiHeader = ''
        tolLoHeader = ''

        valueHeadersAll = columnValues[columnValues.str.contains('value')].to_list()
        valueHeaders = dict()
        for valHeaderIdx, valHeader in enumerate(valueHeadersAll):
            if 'act' in valHeader.lower():
                valueHeaders['Act'] = valHeader
                actualValHeader = valHeader
                unitsLen = valueHeaders['Act'].split('[')[-1].split('/')[0]
                unitsAng = valueHeaders['Act'].split('/')[-1].split(']')[0]
            if 'nom' in valHeader.lower():
                valueHeaders['Nom'] = valHeader
                nomValHeader = valHeader

        nominalHeadersAll = columnValues[columnValues.str.contains('Nominal')].to_list()
        nominalHeaders = dict()
        for nominalHeaderIdx, nominalHeader in enumerate(nominalHeadersAll):
            if 'x' in nominalHeader.lower():
                nominalHeaders['X'] = nominalHeader
            if 'y' in nominalHeader.lower():
                nominalHeaders['Y'] = nominalHeader
            if 'z' in nominalHeader.lower():
                nominalHeaders['Z'] = nominalHeader

        actualHeadersAll = columnValues[columnValues.str.contains('Actual')].to_list()
        actualHeaders = dict()
        for actualHeaderIdx, actualHeader in enumerate(actualHeadersAll):
            if 'x' in actualHeader.lower():
                actualHeaders['X'] = actualHeader
            if 'y' in actualHeader.lower():
                actualHeaders['Y'] = actualHeader
            if 'z' in actualHeader.lower():
                actualHeaders['Z'] = actualHeader

        tolHeadersAll = columnValues[columnValues.str.contains('Tolerance')].to_list()
        tolHeaders = dict()
        for tolHeaderIdx, tolHeader in enumerate(tolHeadersAll):
            if 'hi' in tolHeader.lower():
                tolHeaders['hi'] = tolHeader
                tolHiHeader = tolHeader
            if 'lo' in tolHeader.lower():
                tolHeaders['lo'] = tolHeader
                tolLoHeader = tolHeader

        finalTolHeadersAll = columnValues[columnValues.str.contains('Final tolerance')].to_list()
        finalTolHeaders = dict()
        for finalTolHeaderIdx, finalTolHeader in enumerate(finalTolHeadersAll):
            if 'hi' in finalTolHeader.lower():
                finalTolHeaders['hi'] = finalTolHeader
            if 'lo' in finalTolHeader.lower():
                finalTolHeaders['lo'] = finalTolHeader

        finalColumns = ['Name',
                        nomValHeader,
                        'Unit',
                        tolHiHeader,
                        tolLoHeader,
                        actualValHeader,
                        'Visual tolerance'
                        ]

        newFinalNames = ['Position No.',
                         'Nominal',
                         'Unit',
                         'USL (Tol+)',
                         'LSL (Tol-)',
                         'actual Dim.',
                         'Deviation'
                         ]

        fullResultsDf.loc[:, 'Unit'] = fullResultsDf.loc[:, actualValHeader].str.split(' ', expand=True)[1]

        for valueHeaderKey, valueHeaderLbl in valueHeaders.items():
            fullResultsDf.loc[:, valueHeaderLbl] = fullResultsDf.loc[:, valueHeaderLbl].astype(str).str.replace(',', '').str.split(' ', expand=True)[
                0].astype(float).round(3)

        for tolHeaderKey, tolHeaderLbl in tolHeaders.items():
            fullResultsDf.loc[:, tolHeaderLbl] = \
                fullResultsDf.loc[:, tolHeaderLbl].astype(str).str.replace(',', '').str.split(' ', expand=True)[
                    0].replace('nan', 0).astype(float).round(3)
            # try:
            #     fullResultsDf.loc[:, tolHeaderLbl] = \
            #     fullResultsDf.loc[:, tolHeaderLbl].str.replace(',', '').str.split(' ', expand=True)[
            #         0].fillna(0).astype(float).round(3)
            # except:
            #     fullResultsDf.loc[:, tolHeaderLbl] = \
            #     fullResultsDf.loc[:, tolHeaderLbl].astype(str).str.replace(',', '').str.split(' ', expand=True)[
            #         0].replace('nan', 0).astype(float).round(3)

        for finalTolHeaderKey, finalTolHeaderLbl in finalTolHeaders.items():
            fullResultsDf.loc[:, finalTolHeaderLbl] = fullResultsDf.loc[:, finalTolHeaderLbl].astype(str).str.replace(
                ',', '').astype(float).round(
                3)


        nomPosDict = dict()
        for nominalHeaderKey, nominalHeaderLbl in nominalHeaders.items():
            nomPos = fullResultsDf.filter(['Name', nominalHeaderLbl, actualHeaders[nominalHeaderKey]]
                                          ).dropna(axis=0, subset=[actualHeaders[nominalHeaderKey]])
            nomPos.loc[:, 'Name'] = nomPos.loc[:, 'Name'] + ' --' + nominalHeaderKey + '-- '
            nomPos.rename(columns={nominalHeaderLbl: nomValHeader,
                                   actualHeaders[nominalHeaderKey]: actualValHeader}, inplace=True)
            nomPosDict[nominalHeaderKey] = nomPos

        nomPosDfAll = pandas.Series()
        if nomPosDict:
            nomPosDfAll = pandas.concat(nomPosDict)
            if not nomPosDfAll.empty:
                nomPosDfAll = nomPosDfAll.droplevel(0)
                nomPosDfAll = nomPosDfAll.round(3)
                nomPosDfAll.loc[:, 'Unit'] = unitsLen
            else:
                nomPosDfAll = pandas.Series()

        fullDfFinal = pandas.concat([fullResultsDf, nomPosDfAll])
        fullDfFinal = fullDfFinal.round(3)
        fullDfFinal.loc[:, 'Name'] = 'Feature ' + fullDfFinal.loc[:, 'Name']
        fullDfFinal.reset_index(drop=False, inplace=True)
        fullDfFinal.sort_values(by=['index', 'Name'], inplace=True)
        # fullDfFinal.dropna(subset=['Nom. value [%s/%s]' % (unitsLen, unitsAng)], inplace=True)

        for rowNum, rowSeries in fullDfFinal.iterrows():
            actualVal = rowSeries.loc['Act. value [%s/%s]' % (unitsLen, unitsAng)]
            nominalVal = rowSeries.loc['Nom. value [%s/%s]' % (unitsLen, unitsAng)]
            upTolVal = rowSeries.loc['Tolerance (hi) [%s/%s]' % (unitsLen, unitsAng)]
            lowTolVal = rowSeries.loc['Tolerance (lo) [%s/%s]' % (unitsLen, unitsAng)]
            lowSpec = nominalVal + lowTolVal
            highSpec = nominalVal + upTolVal
            if str(lowSpec) == 'nan' or str(highSpec) == 'nan' or str(actualVal) == 'nan':
                fullDfFinal.loc[rowNum, 'Visual tolerance'] = ''
                continue
            if actualVal > highSpec:
                # Out of spec high
                outOfSpecValHigh = actualVal - (nominalVal + upTolVal)
                fullDfFinal.loc[rowNum, 'Visual tolerance'] = outOfSpecValHigh
                continue
            if actualVal < lowSpec:
                # Out of spec low
                outOfSpecValLow = actualVal - (nominalVal + lowTolVal)
                fullDfFinal.loc[rowNum, 'Visual tolerance'] = outOfSpecValLow
                continue

            # New Method to match VG position exactly (simple replace)
            # 5/10/2021
            newVisTol = fullDfFinal.loc[rowNum, 'Visual tolerance'].replace('_', '-')
            newVisTol = newVisTol.replace('X', '+')
            # newVisTol = newVisTol.replace('.', '-')
            # newVisTol = newVisTol.replace('|', '-')
            newVisTol = newVisTol.replace('.', '')
            newVisTol = newVisTol.replace('|', '')
            newVisTol = "".join(newVisTol.split())
            newVisTol = newVisTol.strip()
            fullDfFinal.loc[rowNum, 'Visual tolerance'] = newVisTol

            # Previous Method poistions based on closest value -
            # Met with Vijay and discussed changing it to match VG exactly to avoid confusion
            # 5/10/2021
            # tolArray = numpy.linspace(lowSpec, highSpec, 13)
            # devArray = pandas.Series(
            #     numpy.abs(float(rowSeries.loc['Act. value [%s/%s]' % (unitsLen, unitsAng)]) - tolArray)).round(8)
            # # devArray.fillna('N/A', inplace=True)
            # minDevIdx = devArray.idxmin()
            # devArrayNew = numpy.full(devArray.shape, fill_value='-', dtype=str)
            # try:
            #     devArrayNew[minDevIdx] = '+'
            # except IndexError:
            #     fullDfFinal.loc[rowNum, 'Visual tolerance'] = ''
            # else:
            #     visualDeviation = ''.join(devArrayNew.tolist())
            #     fullDfFinal.loc[rowNum, 'Visual tolerance'] = visualDeviation

        fullDfFinal.dropna(subset=['Nom. value [%s/%s]' % (unitsLen, unitsAng)], inplace=True)
        fullDfFinal = fullDfFinal.filter(finalColumns)
        renameDict = dict(zip(finalColumns, newFinalNames))
        fullDfFinal.rename(columns=renameDict, inplace=True)
        fullDfFinal.columns = pandas.MultiIndex.from_product([[os.path.basename(resultsFile)], fullDfFinal.columns])
        # fullDfFinal.fillna('', inplace=True)
        fullDfFinal.to_excel(outputFormattedFile)

        if fullDfFinal.shape[0] > 24:
            n = 24
            chunks = fullDfFinal.groupby(numpy.arange(len(fullDfFinal)) // n)
            fullDfFinalList = list()
            for g, df in chunks:
                fullDfFinalList.append(df)
                print(df.shape)
        else:
            fullDfFinalList = [fullDfFinal]

        return fullDfFinalList, outputFormattedFile


    def render_results_table(self, data, fig, ax, cavityNum=None, autoSizeFont=False):

        neededRows = 0
        if data.shape[0] < 24:
            neededRows = 24 - data.shape[0]
            emptyDf = pandas.DataFrame(numpy.nan,
                                       index=range(data.index.max(), data.index.max() + neededRows),
                                       columns=data.columns)
            data = data.append(emptyDf)

        data.fillna('', inplace=True)
        ax.axis('off')
        fileName = data.columns.levels[0].to_list()[0]

        if cavityNum is not None:
            header1 = ['%s (%s)' % (cavityNum, fileName)]
        else:
            header1 = [fileName]
        header2 = data.columns.droplevel(0)
        mpl_table_header = ax.table(cellText=[['']], bbox=(0.0, 0.8, 1.0, 0.2), colLabels=header1)
        mpl_table = ax.table(cellText=data.values, bbox=(0.0, 0.0, 1.0, 0.9), colLabels=header2)

        mpl_table.set_fontsize(10)
        mpl_table_header.set_fontsize(16)

        for cellIdx, cellObj in mpl_table_header._cells.items():
            cellRow = cellIdx[0]
            cellCol = cellIdx[1]
            cellText = cellObj._text.get_text()
            if cellRow == 0:  # and cellText == '':
                # cellObj.set_edgecolor('gray')
                cellObj.set_text_props(weight='bold', color='black', fontsize=16)

        for cellIdx, cellObj in mpl_table._cells.items():
            cellRow = cellIdx[0]
            cellCol = cellIdx[1]
            cellText = cellObj._text.get_text()
            if cellRow == 0:  # and cellText == '':
                # cellObj.set_edgecolor('gray')
                cellObj.set_text_props(weight='bold', color='black', fontsize=12)
            else:
                cellObj.set_facecolor('#F2F2F2')
            if cellCol == 0:
                # cellObj.set_edgecolor('black')
                # cellObj.set_facecolor('white')
                # if '*' in cellText:
                #     textColor = 'red'
                # if '**' in cellText:
                #     textColor = 'blue'
                # cellObj.set_text_props(weight='bold', color=textColor)
                cellObj._loc = 'left'
            elif cellCol == 6:
                cellObj._loc = 'center'
            else:
                cellObj._loc = 'right'
            if (cellCol == 1 or cellCol == 3 or cellCol == 4 or cellCol == 5) and cellRow != 0:
                try:
                    cellObj._text.set_text('%0.3f' % float(cellText))
                except:
                    pass
            if cellCol == 6:
                try:
                    # set deviation column text to red if out of spec and not "---+---"
                    cellObj._text.set_text('%0.3f' % float(cellText))
                    cellObj.set_text_props(color='red')
                except:
                    pass
            if cellRow > (24 - neededRows):
                cellObj.set_facecolor('white')
                cellObj.set_edgecolor('white')
                # cellObj.set_text_props(weight='bold', color='black', fontsize=12)
                # if cellRow == 24:  # last row

        # mpl_table.auto_set_font_size(True)
        mpl_table.auto_set_font_size(autoSizeFont)
        mpl_table.auto_set_column_width(col=list(range(len(data.columns))))  # Provide integer list of columns to adjust

        # self.signalAnalysisThreadLog.emit('Rendering header info table complete.', 'info')
        return fig, ax, mpl_table


    def read_excel_file(self, inputFile, fileInfoKey, cavityNumber):
        self.signalAnalysisThreadLog.emit('Reading excel:  {fullfile}'.format(
            fullfile=inputFile), 'info')

        outputFileList = list()
        excelFig, excelAx = plt.subplots(figsize=(16, 9))

        if fileInfoKey.lower() == 'results table':
            formattedResultsList, formattedResultsExcelFile = self.format_results_data(inputFile, type='excel')
            for formattedResultsDfIdx, formattedResultsDf in enumerate(formattedResultsList):
                outputFile = os.path.join(os.path.dirname(inputFile),
                                          '%s_Page-%i.png' % (os.path.splitext(os.path.basename(inputFile))[0], formattedResultsDfIdx))
                resultFig, csvAx, csvTable = self.render_results_table(formattedResultsDf, excelFig, excelAx, cavityNumber)
                resultFig.savefig(outputFile, facecolor=resultFig.get_facecolor(), edgecolor='none', bbox_inches='tight')
                outputFileList.append(outputFile)
        elif 'histogram' in fileInfoKey.lower():
            outputFile = os.path.join(os.path.dirname(inputFile),
                                      '%s.png' % os.path.splitext(os.path.basename(inputFile))[0])
            excelDf = pandas.read_excel(inputFile)
            excelDf.dropna(axis=1, inplace=True, how='all')
            resultFig, devAx = self.render_deviation_plot(excelDf, excelFig, excelAx)
            resultFig.savefig(outputFile, facecolor=resultFig.get_facecolor(), edgecolor='none', bbox_inches='tight')
            outputFileList.append(outputFile)
        else:
            outputFileList = [inputFile]
        return outputFileList


    def read_csv_file(self, inputFile, fileInfoKey, cavityNumber):
        self.signalAnalysisThreadLog.emit('Reading csv file:  {fullfile}'.format(
            fullfile=inputFile), 'info')

        outputFileList = list()
        csvFig, csvAx = plt.subplots(figsize=(16, 9))

        if fileInfoKey.lower() == 'results table':
            formattedResultsList, formattedResultsCsvFile = self.format_results_data(inputFile, type='csv')
            for formattedResultsDfIdx, formattedResultsDf in enumerate(formattedResultsList):
                outputFile = os.path.join(os.path.dirname(inputFile),
                                          '%s_Page-%i.png' % (os.path.splitext(os.path.basename(inputFile))[0], formattedResultsDfIdx))
                resultFig, csvAx, csvTable = self.render_results_table(formattedResultsDf, csvFig, csvAx, cavityNumber)
                resultFig.savefig(outputFile, facecolor=resultFig.get_facecolor(), edgecolor='none', bbox_inches='tight')
                outputFileList.append(outputFile)
        elif 'histogram' in fileInfoKey.lower():
            outputFile = os.path.join(os.path.dirname(inputFile),
                                      '%s.png' % os.path.splitext(os.path.basename(inputFile))[0])
            csvDf = pandas.read_csv(inputFile)
            csvDf.dropna(axis=1, inplace=True, how='all')
            resultFig, devAx = self.render_deviation_plot(csvDf, csvFig, csvAx)
            resultFig.savefig(outputFile, facecolor=resultFig.get_facecolor(), edgecolor='none', bbox_inches='tight')
            outputFileList.append(outputFile)
        else:
            outputFileList = [inputFile]

        return outputFileList


    def addPopulatedSlide(self, slideName, mapDict, sourceDir):
        sourceDirName = os.path.basename(sourceDir)
        self.signalAnalysisThreadLog.emit('\tAdding %s to PPT Document for directory: %s' % (slideName, sourceDirName), 'info')
        slideToAdd = self.slideLayoutDict[slideName]['slide']
        slide = self.pptPresentation.slides.add_slide(slideToAdd)
        slideDict = dict()
        for slidePH in slide.placeholders:
            slideLoc = (slidePH.top, slidePH.left)
            # slideDict[slideLoc] = {'placeholder': slidePH}
            slideDict[slideLoc] = slidePH

        for contentPH, contentDict in mapDict.items():
            if isinstance(contentDict, dict):
                key = contentDict['Key']
                content = contentDict['Value']
                fileType = contentDict['File Type']
            else:
                key = contentPH
                content = contentDict
                fileType = 'Text'

            placeholderKeys = list(self.slideLayoutDict[slideName]['placeholders'].keys())
            if key not in placeholderKeys:
                continue

            slideToAddPH = self.slideLayoutDict[slideName]['placeholders'][key]
            slidePHKeyLoc = slideToAddPH['loc']
            placeholderItem = slideDict[slidePHKeyLoc]

            if type(placeholderItem) == pptx.shapes.placeholder.PicturePlaceholder:
                    picture = placeholderItem.insert_picture(content)
                    picture.crop_bottom = 0.0
                    picture.crop_top = 0.0
                    picture.crop_left = 0.0
                    picture.crop_right = 0.0

                # pass
            elif type(placeholderItem) == pptx.shapes.placeholder.SlidePlaceholder:
                text = placeholderItem.text = content
            elif type(placeholderItem) == pptx.shapes.placeholder.SlidePlaceholder:
                pass
        return


    def create_powerpoint(self):
        self.signalAnalysisThreadLog.emit('Creating Powerpoint Report from template:  {pptTemplate}'.format(pptTemplate=self.pptTemplate), 'info')
        self.pptPresentation = pptx.Presentation(self.pptTemplate)
        self.slideLayoutDict = dict()
        for counter, slideLayout in enumerate(self.pptPresentation.slide_layouts):
            self.slideLayoutDict[slideLayout.name] = {'index': counter,
                                                      'slide': slideLayout}
        for key, subDict in self.slideLayoutDict.items():
            slide = subDict['slide']
            placeholdersDict = dict()
            for slideLayoutPH in slide.placeholders:
                slideText = slideLayoutPH.text
                slideLoc = (slideLayoutPH.top, slideLayoutPH.left)
                slideHt = slideLayoutPH.height / float(Inches(1))
                slideWd = slideLayoutPH.width / float(Inches(1))
                placeholdersDict[slideText] = {'loc': slideLoc,
                                               'width': slideWd,
                                               'height': slideHt}
            subDict['placeholders'] = placeholdersDict

        for slideNum, slideInfo in self.mapperDictUpdated.items():
            slideToAdd = 'NONE'
            try:
                slideToAdd = slideInfo['Slide Name']
                sampleRepeat = slideInfo['Sample Repeat']
                sourceDir = slideInfo['Source Directory']
                mappingDict = copy.deepcopy(slideInfo)
                del mappingDict['Slide Name']
                del mappingDict['Sample Repeat']
                del mappingDict['Source Directory']
                self.addPopulatedSlide(slideToAdd, mappingDict, sourceDir)
            except:
                errorMsg = traceback.format_exc()
                print(errorMsg)
                self.signalAnalysisThreadLog.emit('Skipping slide due to error: %s' % slideToAdd, 'error')
                continue

        self.outputPPTXReport = os.path.join(self.projectDirectory, '%s_Report.pptx' % os.path.basename(self.projectDirectory))
        self.pptPresentation.save(self.outputPPTXReport)
        # PPTtoPDF(outputPPTXReport, outputPDFReport, formatType=32)


    def main_process(self):
        try:
            self.read_excel_mapper()
            self.update_content_path()
            self.create_powerpoint()
            self.signalAnalysisFileStatus.emit("Completed")
        except:
            errorTraceback = traceback.format_exc()
            print(errorTraceback)
            self.signalAnalysisThreadLog.emit(errorTraceback, 'error')
            self.signalAnalysisFileStatus.emit('Error')

        return


def PPTtoPDF(inputFileName, outputFileName, formatType=32):
    # powerpoint = comtypes.client.CreateObject("Powerpoint.Application")
    powerpoint = win32com.client.Dispatch("Powerpoint.Application")
    powerpoint.Visible = 1

    if outputFileName[-3:] != 'pdf':
        outputFileName = outputFileName + ".pdf"
    deck = powerpoint.Presentations.Open(inputFileName)
    deck.SaveAs(outputFileName, formatType)  # formatType = 32 for ppt to pdf
    deck.Close()
    powerpoint.Quit()


if __name__ == '__main__':

    # --- Test analysis:
    print('\nTesting...')
    projectDir = r"C:\_Data\KempReporting\VG_BATCH_01"
    initialSheetsDir = r"C:\_Data\KempReporting\VG_BATCH_01\Initial Sheets"
    sampleDirPaths = [r"C:\_Data\KempReporting\VG_BATCH_01\Sample Sheets\Sample_01",
                      r"C:\_Data\KempReporting\VG_BATCH_01\Sample Sheets\Sample_02"]
    contentMapFile = r"C:\_Data\KempReporting\VG_BATCH_01\SampleProject_Content Mapper.xlsx"
    headerInfoFile = r"C:\_Data\KempReporting\VG_BATCH_01\SampleProject_Header Info.xlsx"
    pptTemplateFile = r"C:\_Data\KempReporting\_Source_Code\templates\Kemp_Report_TEMPLATE.pptx"
    varianceExpImgFile = r"C:\_Data\KempReporting\_Source_Code\bin\Variance_Explanation.png"
    noImgFoundFile = r"C:\_Data\KempReporting\_Source_Code\bin\NO_IMAGE_PROVIDED.png"

    cmapFile = r"C:\_Data\KempReporting\VG_BATCH_01\colormap.pkl"
    normFile = r"C:\_Data\KempReporting\VG_BATCH_01\colornorm.pkl"

    with open(cmapFile, 'rb') as handle:
        cmap = pickle.load(handle)

    with open(normFile, 'rb') as handle:
        norm = pickle.load(handle)

    reportMaker = ReportCreator()
    reportMaker.projectDirectory = projectDir
    reportMaker.initialSheetsDirectory = initialSheetsDir
    reportMaker.sampleDirPath = sampleDirPaths
    reportMaker.contentMapperFile = contentMapFile
    reportMaker.headerInfoFile = headerInfoFile
    reportMaker.pptTemplate = pptTemplateFile
    reportMaker.varianceExpImgFile = varianceExpImgFile
    reportMaker.noImgFoundFile = noImgFoundFile
    reportMaker.cmapObj = cmap
    reportMaker.normObj = norm
    reportMaker.main_process()

    print('\nDone Testing.')
