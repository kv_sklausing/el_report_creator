@echo off

REM Set ui filename.
set ui_filename_no_ext=ReportCreator_Settings_Window

REM Set path to the pyside2-uic.exe file.
set path_pyside2_uic="C:\Anaconda3\Scripts\pyside2-uic.exe"

REM get the current directory of this .bat file
set bat_file_directory=%~d0%~p0

REM Set path to the Qt ui file.
set path_qt_ui_file="%bat_file_directory%%ui_filename_no_ext%.ui"

REM Set path to the output py file.
set path_py_file="%bat_file_directory%%ui_filename_no_ext%.py"

@echo on

REM Execute commands to convert Qt UI file to python class.
%path_pyside2_uic% %path_qt_ui_file% > %path_py_file%

pause
