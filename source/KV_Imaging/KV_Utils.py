'''
Collection of generic utility functions.
'''


import time
import numpy


def time_function(func):
    '''
    Decorator to calculate time for function execution.
    '''
    def wrapper(*arg, **kwargs):
        timeStart = time.time()
        result = func(*arg, **kwargs)
        timeEnd = time.time()
        print('Total time taken in : %s : %.3f [sec]' % (
            func.__name__, timeEnd-timeStart))
        return result
    return wrapper


def is_empty(objectArg):
    '''
    Test object for an empty sequence, empty array, or None.
    '''
    if objectArg is None:
        return True
    elif isinstance(objectArg, numpy.ndarray):
        return objectArg.size == 0
    else:
        return bool(objectArg)
