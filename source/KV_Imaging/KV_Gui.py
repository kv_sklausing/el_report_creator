'''
Collection of utilities used for gui development.
'''


import os
import traceback
import pickle
from PySide2.QtWidgets import QMessageBox, QFileDialog, QAbstractItemView, QListView, QTreeView
from PySide2.QtGui import QIcon


class WindowSettings():
    # Create empty class for storing window settings.
    def __init__(self, name):
        self.name = name


def load_settings_file(fileFullPathSettings, logger=None):
    if logger is None:
        print('\nLoading settings file...')
    else:
        logger.info('\nLoading settings file...')
    # Check to see if it exists.
    if not os.path.exists(fileFullPathSettings):
        if logger is None:
            print('Settings file does not exist.')
        else:
            logger.info('Settings file does not exist.')
        classSettings = None
        return classSettings
    # Load in the settings file.
    with open(fileFullPathSettings, 'rb') as fileObject:
        classSettings = pickle.load(fileObject)
    if logger is None:
        print('Settings file loaded.')
    else:
        logger.info('Settings file loaded.')
    return classSettings


def save_settings_file(fileFullPathSettings, classSettings, logger=None):
    if logger is None:
        print('\nSaving settings file...')
    else:
        logger.info('\nSaving settings file...')
    # Save settings to file.
    with open(fileFullPathSettings, 'wb') as fileObject:
        pickle.dump(classSettings, fileObject)
    if logger is None:
        print('Settings file saved.')
    else:
        logger.info('Settings file saved.')
    return


def get_settings_filepath(fileFullPathScript):
    # Create the path for the settings file.
    fileNameScript = os.path.basename(fileFullPathScript)
    fileBaseNameScript = os.path.splitext(fileNameScript)[0]
    fileNameSettings = '%s.config' % (fileBaseNameScript)
    fileBasePathScript = os.path.dirname(fileFullPathScript)
    fileFullPathSettings = os.path.join(fileBasePathScript, fileNameSettings)
    return fileFullPathSettings


def open_directory(directoryPath, logger=None):

    if os.path.isfile(directoryPath):
        directoryPath = os.path.dirname(directoryPath)
    else:
        pass
    try:
        # raise NameError('TEST')
        os.startfile(directoryPath)
    except:
        currentException = traceback.format_exc()
        if logger is None:
            print('Could not open project directory:\n%s\n Unexpected error:\n%s' % (directoryPath, currentException))
        else:
            logger.exception('Unexpected error:')
    else:
        if logger is None:
            print('Opened project directory:\n%s' % directoryPath)
        else:
            logger.info('Opened project directory:%s' % directoryPath)


def select_directory_dialog(directoryPath, iconPath=None, mulitDir=False, logger=None):
    if logger is None:
        print('\nInteractively selecting directory...')
    else:
        logger.info('Interactively selecting directory...')
    try:
        # raise NameError('TEST')
        qFileDialog = QFileDialog()
        qFileDialog.setWindowTitle('Select existing directory...')
        qFileDialog.setViewMode(QFileDialog.Detail)
        qFileDialog.setFileMode(QFileDialog.Directory)
        qFileDialog.setDirectory(directoryPath)
        if not iconPath == None:
            qFileDialog.setWindowIcon(QIcon(iconPath))
        if mulitDir:
            qFileDialog.setOption(QFileDialog.DontUseNativeDialog, True)
            file_view = qFileDialog.findChild(QListView, 'listView')
            f_tree_view = qFileDialog.findChild(QTreeView)
            # to make it possible to select multiple directories:
            if file_view:
                file_view.setSelectionMode(QAbstractItemView.MultiSelection)
            if f_tree_view:
                f_tree_view.setSelectionMode(QAbstractItemView.MultiSelection)

        if qFileDialog.exec_():
            # Update project directory.
            directoryPath = qFileDialog.selectedFiles()[0]
            if logger is None:
                print('Directory selected:\n%s' % directoryPath)
            else:
                logger.info('Directory selected: %s' % directoryPath)
            return directoryPath
        else:
            if logger is None:
                print('Directory selection cancelled.')
            else:
                logger.info('Directory selection cancelled.')
            return None
    except:
        currentException = traceback.format_exc()
        if logger is None:
            print('Issue selecting directory.\n Unexpected error:\n%s' % currentException)
        else:
            logger.exception('Unexpected error: ')



def select_file_dialog(filePathStart, iconPath=None, logger=None, title=None, FILE_TYPES='PPT Files (*.pptx *.ppt)'):
    filePath = None
    if logger is None:
        print('\nInteractively selecting file...')
    else:
        logger.info('Interactively selecting file...')
    try:
        # raise NameError('TEST')
        qFileDialog = QFileDialog()
        if title is None:
            qFileDialog.setWindowTitle('Select existing file...')
        else:
            qFileDialog.setWindowTitle(title)
        if not iconPath == None:
            qFileDialog.setWindowIcon(QIcon(iconPath))
        qFileDialog.setViewMode(QFileDialog.Detail)
        qFileDialog.setFileMode(QFileDialog.ExistingFile)
        qFileDialog.setDirectory(os.path.dirname(filePathStart))
        qFileDialog.setNameFilters([FILE_TYPES])
        if qFileDialog.exec_():
            # Update project file.
            filePath = qFileDialog.selectedFiles()[0]
            if logger is None:
                print('File selected:\n%s' % filePath)
            else:
                logger.info('File selected:\n%s' % filePath)
        else:
            filePath = filePathStart
            if logger is None:
                print('File selection cancelled.')
            else:
                logger.info('File selection cancelled.')
    except:
        currentException = traceback.format_exc()
        if logger is None:
            print('Issue selecting file.\n Unexpected error:\n%s' % currentException)
        else:
            logger.exception('Unexpected error:')
    return filePath


def warn_user_dialog(message, iconPath=None, logger=None):
    if logger is None:
        print('\nWarning user via message box...')
    else:
        logger.warning('Warning user via message box...')
    qMessageBox = QMessageBox()
    qMessageBox.setWindowTitle('Warning...')
    if not iconPath == None:
        qMessageBox.setWindowIcon(QIcon(iconPath))
    qMessageBox.setIcon(QMessageBox.Warning)
    qMessageBox.setText(message)
    if logger is not None:
        logger.warning(message)
    qMessageBox.exec_()
    return


def alert_user_dialog(message, iconPath=None, informText=None, detailText=None, logger=None):
    if logger is None:
        print('\nAlerting user via message box...')
    else:
        logger.warning('Alerting user via message box...')
    qMessageBox = QMessageBox()
    qMessageBox.setWindowTitle('Info...')
    if not iconPath == None:
        qMessageBox.setWindowIcon(QIcon(iconPath))
    qMessageBox.setIcon(QMessageBox.Information)
    qMessageBox.setText(message)
    if informText:
        qMessageBox.setInformativeText(informText)
        if logger is not None:
            logger.warning(informText)
    if detailText:
        qMessageBox.setDetailedText(detailText)
        if logger is not None:
            logger.warning(detailText)
    qMessageBox.exec_()
    return
