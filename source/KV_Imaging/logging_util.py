"""
The Kinetic Vision Imaging Group library (KVIMG)

This code is developed, maintained, and owned by Kinetic Vision with the intent
of aiding imaging engineers in typical project work.

These utilities are intended to help logging of scripts

"""


import os
import sys
import logging
import threading
import multiprocessing

KV_LOGGER_NAME = 'kv_logger'
LOG_FILE_NAME = 'kv_log.log'

# Some default formatting styles
#   Idea that console doesnt need time stamps
FORMAT_FILE = '%(asctime)s %(levelname)s: %(message)s'
FORMAT_CONSOLE = '%(levelname)s: %(message)s'

# Default levels for different object types
DEFAULT_LEVEL_LOGGER = logging.DEBUG
DEFAULT_LEVEL_FILE = logging.DEBUG
DEFAULT_LEVEL_CONSOLE = logging.INFO


#####
#   Quality of life functions
#####
def init_logger(loggerName=KV_LOGGER_NAME, toFile=False, toConsole=False, **kwargs):
    """Simple start for making a logger.
    Will write logs to console and to a file.
    """

    logger = make_logger(loggerName=loggerName, **kwargs)

    if toFile:
        add_file_handler(logger, **kwargs)

    if toConsole:
        add_console_handler(logger, **kwargs)

    return logger

def shutdown_logger(logger):
    """Closes and removes all handlers for a logger."""

    logger = make_logger_name_check(logger)

    handlers = logger.handlers[:]
    for handler in handlers:
        handler.close()
        logger.removeHandler(handler)

    return

#####
#   Make loggers and add handlers
#####
def is_logger(logger):
    """Check if object is a logging object"""
    return isinstance(logger, logging.Logger)

def make_logger_name_check(logger):
    """Make-get a logger based on input.

    If input is a Logger, return it
    If input is a str, get-make logger with that name
    """

    if is_logger(logger):
        return logger

    else:
        loggerObj = logging.getLogger(logger)
        return loggerObj


def make_logger(loggerName=KV_LOGGER_NAME, loggerLevel=DEFAULT_LEVEL_LOGGER, **kwargs):
    """Makes a new logger object to log with.
    Useful to make custom loggers that will not log with 3rd pary modules.

    Inputs:
        - loggerName : str (kv_logger)
            Name of logger to get or make.
            If None, will return root logger (one used by logging.debug, etc)
        - loggerLevel : str | int (None)
            Min level the logger will care about.
            Ex values include: logging.DEBUG
            If None, level will not be set
    """

    # makes unique logger (ensures 3rd party modules are not also logged)
    logger = logging.getLogger(loggerName)

    if loggerLevel is not None:
        logger.setLevel(loggerLevel)

    return logger

def add_file_handler(logger, fileLevel=DEFAULT_LEVEL_FILE, fileFormat=FORMAT_FILE,
                     logPath=None, logFileName=None, logDir=None, logFileMode='w',
                     forMultiprocess=False, **kwargs):
    """Adds a file handler to a logger.

    Any logs with equal to or greater fileLevel will be written to the file

    Parameters
    ----------
        logger : Logger | str
            Logger object to add handler to, or string for logger's name

        fileLevel : str | int
            Min logging level to write

        fileFormat : str
            How to format log message to file

        logPath : str (None)
            Full path to desired output log file.
            If None, will try to build path from other args.

        logFileName : str (None)
            Name of log file to use. Only considered if logPath not given.
            Will have default name if only logDir is given

        logDirNlogDirame : str (None)
            Name of directory to save log file to.
            Only considered if logPath not given.
            Will use current working dir if None is given.

        logFileMode : str 'w'
            Mode to write with
            'w': overwrite file
            'a': append to file

        forMultiprocessing : boolean (False)
            File handler will allow for multiprocessing to save to same log file.
    """

    # get correct file path to use
    if logPath is None:
        if not os.path.isdir(str(logDir)):
            logDir = os.getcwd()

        if logFileName is None:
            logFileName = LOG_FILE_NAME

        logPath = os.path.join(logDir, logFileName)

    else:
        # log path was already given
        pass

    # get the logger to add the handler to
    #logger = logging.getLogger(loggerName)
    logger = make_logger_name_check(logger)

    # adds handler so ERROR and CRITICAL logs will go to file
    if forMultiprocess:
        handlerFileOut = MultiprocessFileHandler(logPath, mode=logFileMode)
    else:
        handlerFileOut = logging.FileHandler(logPath, mode=logFileMode)


    handlerFileOut.setLevel(fileLevel)

    formatterFile = logging.Formatter(fileFormat)

    handlerFileOut.setFormatter(formatterFile)
    logger.addHandler(handlerFileOut)

    return

def add_console_handler(logger, consoleLevel=DEFAULT_LEVEL_CONSOLE, consoleFormat=FORMAT_CONSOLE, **kwargs):
    """Adds a console handler to a logger.

    Any logs with equal to or greater consoleLevel will be written to the sys.stdout

    Inputs:
        - logger : Logger | str
            Logger object to add handler to, or string for logger's name
        - consoleLevel : str | int
            Min logging level to write
        - consoleFormat : str
            How to format log message to console
    """

    # get the logger to add the handler to
    #logger = logging.getLogger(loggerName)
    logger = make_logger_name_check(logger)

    handlerStdOut = logging.StreamHandler(sys.stdout)
    handlerStdOut.setLevel(consoleLevel)

    formatterConsole = logging.Formatter(consoleFormat)
    handlerStdOut.setFormatter(formatterConsole)
    logger.addHandler(handlerStdOut)

    return


def save_log(savePath, message=None, **kwargs):
    """Save  log

    Parameters
    ----------
        savePath : str
            Path to save exception log file

        sysEx
            System exit string

        traceB
            Traceback of exception

        message : str (None)
            Optional message to give some context to exception
    """

    try:
        message = str(message)
    except:
        message = 'Unhandled exception encountered'

    with open(savePath, 'w') as fout:
        fout.write(message)
        fout.write('\n')
        fout.close()

    return


#####
#   Custom handlers
#####
class MultiprocessFileHandler(logging.Handler):
    """Multiprocessing log handler

    This handler makes it possible for several processes
    to log to the same file by using a queue.

    Code pulled on 190917 from:
    https://mattgathu.github.io/multiprocessing-logging-in-python/

    """

    def __init__(self, fname, **kwargs):
        logging.Handler.__init__(self)

        self._handler = logging.FileHandler(fname, **kwargs)
        self.queue = multiprocessing.Queue(-1)

        thrd = threading.Thread(target=self.receive)
        thrd.daemon = True
        thrd.start()

    def setFormatter(self, fmt):
        logging.Handler.setFormatter(self, fmt)
        self._handler.setFormatter(fmt)

    def receive(self):
        while True:
            try:
                # send record to a queue first
                record = self.queue.get()

                # send to file using parent FileHandler function
                self._handler.emit(record)
            except (KeyboardInterrupt, SystemExit):
                raise
            except EOFError:
                break
            except:
                traceback.print_exc(file=sys.stderr)

    def send(self, s):
        """Puts formatted  record into the queue"""
        self.queue.put_nowait(s)

    def _format_record(self, record):
        if record.args:
            record.msg = record.msg % record.args
            record.args = None
        if record.exc_info:
            dummy = self.format(record)
            record.exc_info = None

        return record

    def emit(self, record):
        try:
            # format record and send to quee
            formatRecord = self._format_record(record)
            self.send(formatRecord)
        except (KeyboardInterrupt, SystemExit):
            raise
        except:
            self.handleError(record)

    def close(self):
        self._handler.close()
        logging.Handler.close(self)


#####
# Exception handling
#####
def save_exception_log(savePath, sysEx, traceB, message=None, **kwargs):
    """Save exception log

    Parameters
    ----------
        savePath : str
            Path to save exception log file

        sysEx
            System exit string

        traceB
            Traceback of exception

        message : str (None)
            Optional message to give some context to exception
    """

    try:
        message = str(message)
    except:
        message = 'Unhandled exception encountered'

    with open(savePath, 'w') as fout:
        fout.write('Exception encountered while processing\n')
        fout.write(message)
        # TODO: get exception info from sys.exc_info
        #fout.write('\n\n\tException info reported:\n')
        #fout.write(sysEx)
        fout.write('\n')
        fout.write(traceB)
        fout.close()

    return


def test_main(logDir):

    #logDir = r'E:\data'
    if not os.path.isdire(logDir):
        print('Directory not found!')
        return

    myL = init_logger(logDir=logDir)

    myL.debug('This is a debug')
    myL.info('This is an info')
    myL.warning('This is a warning')
    myL.error('This is an error')
    myL.critical('This is a critical')


    return


if __name__ == '__main__':

    test_main()

