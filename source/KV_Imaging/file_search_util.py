import os
import string


def get_files(directory, dataTypeList, contains=None, location='any', logger=None):  # location = 'any' or 'end'
    """Summary of function.

    Returns a list of files found based on the the search directory, type of file given and a specific end string (if any).

    Args:
        directory: search directory where files should be found
        dataTypeList: type = list, file extensions for valid files desired
        contains: type = string, a string that is in the files you want, identifier
        locatoin: type = string, 'any' used for the contain string to be anywhere in the file, 'end' to have the file specifically end with the string

    Returns:
        A list of files found

    Raises:
        No raises, returns None if no files found based on input arguments

    Example Usage:
        get_files(input_directory, ['.stl'], endsWithStr='Baseline')

    """

    if contains is None:
        contains = ''

    if logger is None:
        print("\nSearching for %s.%s files in %s" % (contains, str(dataTypeList), directory))
    else:
        logger.info("Searching for %s.%s files in %s" % (contains, str(dataTypeList), directory))

    # Filter for finding valid file
    def data_filter(file):
        fileNameNoExt, fileExt = os.path.splitext(file)
        goodFile = False
        if fileExt in dataTypeList:
            if contains != None:
                if isinstance(contains, str):
                    if location == 'end':
                        goodFile = fileNameNoExt.lower().endswith(contains.lower())
                    elif location == 'any':
                        if contains.lower() in fileNameNoExt.lower():
                            goodFile = True
                        else:
                            goodFile = False
                elif isinstance(contains, list) or isinstance(contains, tuple):
                    for strMatch in contains:
                        if location == 'end':
                            goodFile = fileNameNoExt.lower().endswith(strMatch.lower())
                        elif location == 'any':
                            if strMatch.lower() in fileNameNoExt.lower():
                                goodFile = True
                                break
                            else:
                                goodFile = False
            else:
                goodFile = True
        else:
            goodFile = False
        return goodFile

    # Create list of all files in directory
    if not os.path.isdir(directory):  # check input
        if logger is None:
            print('Directory does not exist.\n%s' % directory)
        else:
            logger.error('Directory does not exist.%s' % directory)
        return None
    else:
        all_files = os.listdir(directory)  # get all files in directory
        data_files = list(filter(data_filter, all_files))  # filter out everything that isnt a valid data file
        data_files = [os.path.join(directory, validData) for validData in data_files]  # create full file path

        if len(data_files) == 0:  # error if no valid data types
            if logger is None:
                print('No valid files %s.%s in %s' % (contains, str(dataTypeList), directory))
            else:
                logger.warning('No valid files %s.%s in %s' % (contains, str(dataTypeList), directory))
            return None

    if logger is None:
        print("Found %i %s.%s files in %s." % (len(data_files), contains, str(dataTypeList), directory))
    else:
        logger.info("Found %i %s.%s files in %s." % (len(data_files), contains, str(dataTypeList), directory))

    return data_files


def valid_dirs_in_directory(directory, logger=None):
    """Summary of function.

    Function that finds valid folders within a given directory

    Args:
        directory: Path to a directory

    Returns:
        list of file paths to a valid directories found

    Raises:
        Returns empty if input direcotry is None

    Example Usage:
        (optional, but recommended; write an example line calling the function)
    """
    dirs_in_directory = []
    if directory == None:
        return directory
    else:
        directoryitems = os.listdir(directory)
        for item in directoryitems:
            tempdir = os.path.join(directory, item)
            if os.path.isdir(tempdir):
                dirs_in_directory.append(tempdir)
                if logger is None:
                    print('\nFound subdirectory: %s' % item)
                else:
                    logger.info('Found subdirectory: %s' % item)
            else:
                pass
    return (dirs_in_directory)

