import sys
import os

from PySide2.QtWidgets import QMainWindow, QApplication, QStyleFactory
from PySide2.QtCore import Qt
from PySide2.QtGui import QIcon

import KV_Imaging as kvimg

from ReportCreator_Settings_Window import Ui_Dialog as ParameterWindowUi


# Define module constants.
# Get info about this file.
if getattr(sys, 'frozen', False):
    # frozen
    SCRIPT_FULLPATH = sys.executable
    print(SCRIPT_FULLPATH)
else:
    # unfrozen
    SCRIPT_FULLPATH = __file__
SCRIPT_DIRECTORY = os.path.dirname(SCRIPT_FULLPATH)
SCRIPT_FILENAME = os.path.basename(SCRIPT_FULLPATH)

MAIN_DIRECTORY = os.path.dirname(SCRIPT_DIRECTORY)

# Bin folder containing neccesary files for Report Creator to work.
BIN_DIRECTORY = os.path.join(MAIN_DIRECTORY, 'bin')

# Template folder containing neccesary files for Report Creator to work.
TEMPLATE_DIRECTORY = os.path.join(MAIN_DIRECTORY, 'templates')

# Public AppData folder. Windows users should have write access here.
APP_DATA_DIRECTORY = r'C:\Users\Public\AppData\KV_ReportCreator'
if not os.path.isdir(APP_DATA_DIRECTORY):
    os.makedirs(APP_DATA_DIRECTORY)

# Define user settings file.
SETTINGS_FILENAME = 'Report Creator Template Settings.config'
SETTINGS_FULLPATH = os.path.join(APP_DATA_DIRECTORY, SETTINGS_FILENAME)


# Define Variance Explanation file.
DEFAULT_VAR_EXPLAIN_FILE = os.path.join(BIN_DIRECTORY, 'Variance_Explanation.png')
if os.path.isfile(DEFAULT_VAR_EXPLAIN_FILE):
    print(DEFAULT_VAR_EXPLAIN_FILE)
else:
    print("Missing Default Variance Explanation File")

# Define Invalid Image file.
DEFAULT_INVALID_IMG_FILE = os.path.join(BIN_DIRECTORY, 'NO_IMAGE_PROVIDED.png')
if os.path.isfile(DEFAULT_INVALID_IMG_FILE):
    print(DEFAULT_INVALID_IMG_FILE)
else:
    print("Missing Default Invalid Image File")

# Define powerpoint template file.
DEFAULT_POWERPOINT_TEMPLATE_FILE = os.path.join(TEMPLATE_DIRECTORY, 'Kemp_Report_TEMPLATE.pptx')
if os.path.isfile(DEFAULT_POWERPOINT_TEMPLATE_FILE):
    print(DEFAULT_POWERPOINT_TEMPLATE_FILE)
else:
    print("Missing Default Powerpoint Template File")

VALID_PPT_EXT = ['.pptx', '.ppt']
VALID_IMG_EXT = ['.png', '.jpg', '.jpeg']

class ParameterWindow(QMainWindow, ParameterWindowUi):
    '''
    Class for the Report Creator parameter sub window.
    '''

    def __init__(self, logger=None, icon=None):
        # Initialize the parent classes.
        super().__init__()

        # Get Logger
        self.reportSettingsLogger = logger

        # Set Icon
        self.icon = icon

        # Initialize the widgets from the ui designer file.
        # NOTE: this adds all of our widgets from the ui file to the ParameterWindow class.
        self.setupUi(self)

        # Initialize window properties.
        self.init_window()

        # Initialize window settings.
        self.init_window_settings()

        # Load and apply settings from file.
        self.load_settings_from_file()

        # Apply settings for the window.
        self.apply_window_settings()

        # Initialize widget actions.
        self.init_widget_actions()

        return

    def init_window(self):
        # Set some options for the window.
        self.setWindowTitle('Report Creator Settings')
        self.setWindowIcon(QIcon(self.icon))
        self.setFixedSize(self.size())
        self.setWindowModality(Qt.ApplicationModal)

        return

    def init_widget_actions(self):
        # Connect the widgets to their actions.
        """ Powerpoint Report File Actions"""
        ### Select Ppt Template
        self.pushButtonPptTemplate.clicked.connect(
            self.select_ppt_file_action
        )
        ### Update Ppt Template
        self.lineEditPptTemplate.editingFinished.connect(
            self.update_ppt_template_file
        )

        # """ Variance Explanation File Actions"""
        # ### Select Ppt Template
        # self.pushButtonVarImageFile.clicked.connect(
        #     self.select_var_explain_file_action
        # )
        # ### Update Ppt Template
        # self.lineEditVarImageFile.editingFinished.connect(
        #     self.update_var_explain_file
        # )

        """ Invalid Image File Actions"""
        ### Select Ppt Template
        self.pushButtonInvalidImage.clicked.connect(
            self.select_invalid_img_file_action
        )
        ### Update Ppt Template
        self.lineEditInvalidImage.editingFinished.connect(
            self.update_invalid_img_file
        )

        """ Okay Defaults Action """
        self.pushButtonSetParameters.clicked.connect(
            self.closeParametersOkay
        )
        """ Okay Cancel Action """
        self.pushButtonCancelParameters.clicked.connect(
            self.closeParametersCancel
        )
        return

    def init_window_settings(self):
        # Create settings for window.
        self.settingsParameter = kvimg.WindowSettings('ReportCreatorSettings')

        # Default Ppt Tempalte
        self.settingsParameter.defaultPowerpointTemplate = DEFAULT_POWERPOINT_TEMPLATE_FILE
        self.settingsParameter.userPowerpointTemplate = None
        # Default variance explanation image
        self.settingsParameter.defaultVarExplainFile = DEFAULT_VAR_EXPLAIN_FILE
        self.settingsParameter.userVarExplainFile = None
        # Default invalid image
        self.settingsParameter.defaultInvalidImgFile = DEFAULT_INVALID_IMG_FILE
        self.settingsParameter.userInvalidImgFile = None
        return

    def apply_window_settings(self):
        """ Apply setting to widgets for the window."""
        # Widget: lineEditPptTemplate
        if self.settingsParameter.userPowerpointTemplate is not None:
            self.lineEditPptTemplate.setText(
                self.settingsParameter.userPowerpointTemplate)
        else:
            self.lineEditPptTemplate.setText(
                self.settingsParameter.defaultPowerpointTemplate)

        # # Widget: lineEditVarImageFile
        # if self.settingsParameter.userVarExplainFile is not None:
        #     self.lineEditVarImageFile.setText(
        #         self.settingsParameter.userVarExplainFile)
        # else:
        #     self.lineEditVarImageFile.setText(
        #         self.settingsParameter.defaultVarExplainFile)

        # Widget: lineEditInvalidImage
        if self.settingsParameter.userInvalidImgFile is not None:
            self.lineEditInvalidImage.setText(
                self.settingsParameter.userInvalidImgFile)
        else:
            self.lineEditInvalidImage.setText(
                self.settingsParameter.defaultInvalidImgFile)
        return

    def store_window_settings(self):
        """ Retrieve the settings from the widgets for the window."""
        # Widget: lineEditPptTemplate
        self.settingsParameter.userPowerpointTemplate = self.lineEditPptTemplate.text()
        # # Widget: lineEditVarImageFile
        # self.settingsParameter.userVarExplainFile = self.lineEditVarImageFile.text()
        # Widget: lineEditInvalidImage
        self.settingsParameter.userInvalidImgFile = self.lineEditInvalidImage.text()

        return

    def load_settings_from_file(self):
        # Load the settings from file.
        if os.path.isfile(SETTINGS_FULLPATH):
            self.settingsParameter = kvimg.load_settings_file(SETTINGS_FULLPATH)
            self.apply_window_settings()
            self.reportSettingsLogger.info('Report Creator Settings Loaded From File.')
            # # Apply settings for parameter window.
            # self.parameterWindow.settingsParameter = settingsAll.settingsParameter
            # self.parameterWindow.apply_window_settings()
        return

    def save_settings_to_file(self):
        # Get the latest settings.
        self.store_window_settings()
        # Save the settings file.
        kvimg.save_settings_file(SETTINGS_FULLPATH, self.settingsParameter)
        self.reportSettingsLogger.info('Report Creator Settings Saved To File.')
        return

    def closeEvent(self, event):
        # Standard Event when window closes anyway
        # Make sure the settings reflect the default inputs.
        self.store_window_settings()
        self.reportSettingsLogger.info('Report Creator Settings Closed')
        pass
        return

    def closeParametersCancel(self, event):
        # Make sure the settings reflect the default inputs.
        self.apply_default_settings()
        self.close()
        return

    def closeParametersOkay(self, event):
        # Make sure the settings reflect the latest user inputs.
        self.reportSettingsLogger.info('Storing Report Settings.')
        self.save_settings_to_file()
        self.close()
        return

    def apply_default_settings(self):

        """ Apply defualt settings to widgets for the window. """
        # Widget: lineEditPptTemplate
        self.lineEditPptTemplate.setText(
            self.settingsParameter.defaultPowerpointTemplate)

        # # Widget: lineEditVarImageFile
        # self.lineEditVarImageFile.setText(
        #     self.settingsParameter.defaultVarExplainFile)

        # Widget: lineEditInvalidImage
        self.lineEditInvalidImage.setText(
            self.settingsParameter.defaultInvalidImgFile)


        return

    def select_ppt_file_action(self):
        # Choose starting filePath to browse from.
        filePath = self.lineEditPptTemplate.text()
        isBlank = (filePath == '') or (filePath is None)
        if isBlank:
            filePath = self.settingsMain.defaultPowerpointTemplate
        # Ask user to select filePath.
        filePath = kvimg.select_file_dialog(
            filePath, iconPath=self.icon, title='Powerpoint Template File:', FILE_TYPES='PPT Files (*.pptx *.ppt)')
        # Update filePath.
        self.lineEditPptTemplate.setText(filePath)
        self.update_ppt_template_file()
        return

    def update_ppt_template_file(self):
        # Get the text from the line edit widget.
        strLineEditText = self.lineEditPptTemplate.text()
        # Remove white space and quotes.
        strLineEditText = strLineEditText.strip()
        strLineEditText = strLineEditText.strip('\"')
        # Update filePath if line edit input is not blank.
        isBlank = (strLineEditText == '') or (strLineEditText is None)
        if not isBlank:
            strLineEditText = os.path.abspath(strLineEditText)
            # Make sure the filePath exists.
            if not os.path.isfile(strLineEditText):
                # Update filePath line edit widget.
                self.lineEditPptTemplate.setText(" ")
                message = 'Please enter existing file.'
                print(message)
                kvimg.warn_user_dialog(message, iconPath=self.icon)
            elif os.path.splitext(strLineEditText)[-1].lower() not in VALID_PPT_EXT:
                # Update filePath line edit widget.
                self.lineEditPptTemplate.setText(" ")
                message = 'Please enter existing pptx file.'
                print(message)
                kvimg.warn_user_dialog(message, iconPath=self.icon)
            else:
                # Update filePath line edit widget.
                self.lineEditPptTemplate.setText(strLineEditText)
                message = 'Input file set as:\n%s' % (strLineEditText)
                print(message)
        return

    # def select_var_explain_file_action(self):
    #     # Choose starting filePath to browse from.
    #     filePath = self.lineEditVarImageFile.text()
    #     isBlank = (filePath == '') or (filePath is None)
    #     if isBlank:
    #         filePath = self.settingsMain.defaultVarExplainFile
    #     # Ask user to select filePath.
    #     filePath = kvimg.select_file_dialog(
    #         filePath, iconPath=self.icon, title='Variance Explanation Image:', FILE_TYPES='Image Files (*.jpg *.png)')
    #     # Update filePath.
    #     self.lineEditVarImageFile.setText(filePath)
    #     self.update_var_explain_file()
    #     return
    #
    # def update_var_explain_file(self):
    #     # Get the text from the line edit widget.
    #     strLineEditText = self.lineEditVarImageFile.text()
    #     # Remove white space and quotes.
    #     strLineEditText = strLineEditText.strip()
    #     strLineEditText = strLineEditText.strip('\"')
    #     # Update filePath if line edit input is not blank.
    #     isBlank = (strLineEditText == '') or (strLineEditText is None)
    #     if not isBlank:
    #         strLineEditText = os.path.abspath(strLineEditText)
    #         # Make sure the filePath exists.
    #         if not os.path.isfile(strLineEditText):
    #             # Update filePath line edit widget.
    #             self.lineEditVarImageFile.setText(" ")
    #             message = 'Please enter existing file.'
    #             print(message)
    #             kvimg.warn_user_dialog(message, iconPath=self.icon)
    #         elif os.path.splitext(strLineEditText)[-1].lower() not in VALID_IMG_EXT:
    #             # Update filePath line edit widget.
    #             self.lineEditVarImageFile.setText(" ")
    #             message = 'Please enter existing image file.'
    #             print(message)
    #             kvimg.warn_user_dialog(message, iconPath=self.icon)
    #         else:
    #             # Update filePath line edit widget.
    #             self.lineEditVarImageFile.setText(strLineEditText)
    #             message = 'Input file set as:\n%s' % (strLineEditText)
    #             print(message)
    #     return

    def select_invalid_img_file_action(self):
        # Choose starting filePath to browse from.
        filePath = self.lineEditInvalidImage.text()
        isBlank = (filePath == '') or (filePath is None)
        if isBlank:
            filePath = self.settingsMain.defaultInvalidImgFile
        # Ask user to select filePath.
        filePath = kvimg.select_file_dialog(
            filePath, iconPath=self.icon, title='Invalid Image Placeholder:', FILE_TYPES='Image Files (*.jpg *.png)')
        # Update filePath.
        self.lineEditInvalidImage.setText(filePath)
        self.update_invalid_img_file()
        return

    def update_invalid_img_file(self):
        # Get the text from the line edit widget.
        strLineEditText = self.lineEditInvalidImage.text()
        # Remove white space and quotes.
        strLineEditText = strLineEditText.strip()
        strLineEditText = strLineEditText.strip('\"')
        # Update filePath if line edit input is not blank.
        isBlank = (strLineEditText == '') or (strLineEditText is None)
        if not isBlank:
            strLineEditText = os.path.abspath(strLineEditText)
            # Make sure the filePath exists.
            if not os.path.isfile(strLineEditText):
                # Update filePath line edit widget.
                self.lineEditInvalidImage.setText(" ")
                message = 'Please enter existing file.'
                print(message)
                kvimg.warn_user_dialog(message, iconPath=self.icon)
            elif os.path.splitext(strLineEditText)[-1].lower() not in VALID_IMG_EXT:
                # Update filePath line edit widget.
                self.lineEditInvalidImage.setText(" ")
                message = 'Please enter existing image file.'
                print(message)
                kvimg.warn_user_dialog(message, iconPath=self.icon)
            else:
                # Update filePath line edit widget.
                self.lineEditInvalidImage.setText(strLineEditText)
                message = 'Input file set as:\n%s' % (strLineEditText)
                print(message)
        return


if __name__ == '__main__':
    # Add support for multiprocessing for when code has been frozen as Windows executable.
    # https://docs.python.org/2/library/multiprocessing.html#miscellaneous
    # freeze_support()

    # Enable dpi scaling so if the windows system setting for scale is large than 100%
    # the application window scales accordingly without getting distorted.
    QApplication.setAttribute(Qt.AA_EnableHighDpiScaling, on=True)

    # Start the application.
    print('\nApplication started.')
    app = QApplication(sys.argv)

    # Set the main window style. Use windowsvista for newest windows style.
    # print(QStyleFactory.keys())
    # app.setStyle(QStyleFactory.create('windowsvista'))
    # app.setStyle(QStyleFactory.create('Windows'))
    app.setStyle(QStyleFactory.create('Fusion'))

    # Initialize and show the main window.
    mainGui = ParameterWindow()
    mainGui.show()
    app.exit(app.exec_())
    print('\nApplication closed.')





