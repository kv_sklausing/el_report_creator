
from PySide2.QtWidgets import QDialog
from PySide2.QtGui import QIcon

import logging
import KV_Imaging as kvimg

from ReportCreator_Logger_Window import Ui_Dialog as LoggingUi


"""
info(msg, *args, **kwargs)
Logs a message with level INFO on this logger. The arguments are interpreted as for debug().

warning(msg, *args, **kwargs)
Logs a message with level WARNING on this logger. The arguments are interpreted as for debug().

critical(msg, *args, **kwargs)
Logs a message with level CRITICAL on this logger. The arguments are interpreted as for debug().

exception(msg, *args, **kwargs)
Logs a message with level ERROR on this logger. The arguments are interpreted as for debug(). Exception info is added to the logging message. This method should only be called from an exception handler.
"""

KV_LOGGER_NAME = 'ReportCreatorLogger'

class LoggingWindow(QDialog, LoggingUi):
    '''
    Class for the Colorbar Creator main application window.
    '''

    def __init__(self, icon=None):
        # Initialize the parent classes.
        super().__init__()

        # Initialize the widgets from the ui designer file.
        # NOTE: this adds all of our widgets from the ui file to the MainWindow class.
        self.setupUi(self)

        # Set Readonly
        self.plainTextEditLogger.setReadOnly(True)

        self.icon = icon

        self.init_window()

    def init_window(self):
        # Set some options for the window.
        self.setWindowTitle('KV Report Creator - Log Window')
        self.setWindowIcon(QIcon(self.icon))
        return

class LoggingHanlder(logging.Handler):
    def __init__(self, logWindow):

        # Initialize parent classes.
        super().__init__()

        # Initialize log window
        self.logWindow = logWindow

        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        self.setFormatter(formatter)

        logLevel = logging.INFO
        self.setLevel(logLevel)

    def emit(self, record):
        msg = self.format(record)
        self.logWindow.appendPlainText(msg)


class LoggingMain:
    """
    Class that handles processing the analysis routine in its own thread.
    """
    def __init__(self, logWindow, logName):
        # Initialize the parent class.
        super().__init__()

        # Set thread name.
        self.name = 'ReportCreator_Logging'

        self.mainLogger = kvimg.init_logger(loggerName=logName, toFile=False, toConsole=True)
        # self.mainProcessLogger = kvimg.init_logger(loggerName='Processor', toFile=False, toConsole=True)
        # self.settingsLogger = kvimg.init_logger(loggerName='Settings', toFile=False, toConsole=True)
        # self.colorbarLogger = kvimg.init_logger(loggerName='Colorbar', toFile=False, toConsole=True)
        # self.competeLogger = kvimg.init_logger(loggerName='Completion', toFile=False, toConsole=True)

        # Initialize the log window.
        self.logWindow = logWindow

        # Initialize Logging Handler
        self.logHandler = LoggingHanlder(self.logWindow.plainTextEditLogger)

        # Add Handler to Logger
        self.mainLogger.addHandler(self.logHandler)
        # self.mainProcessLogger.addHandler(self.logHandler)
        # self.settingsLogger.addHandler(self.logHandler)
        # self.colorbarLogger.addHandler(self.logHandler)
        # self.competeLogger.addHandler(self.logHandler)

        return
