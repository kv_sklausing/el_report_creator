# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'ReportCreator_Complete_Window.ui'
##
## Created by: Qt User Interface Compiler version 5.15.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_DialogComplete(object):
    def setupUi(self, DialogComplete):
        if not DialogComplete.objectName():
            DialogComplete.setObjectName(u"DialogComplete")
        DialogComplete.resize(292, 107)
        self.pushButtonComplete = QPushButton(DialogComplete)
        self.pushButtonComplete.setObjectName(u"pushButtonComplete")
        self.pushButtonComplete.setGeometry(QRect(36, 60, 101, 31))
        self.label = QLabel(DialogComplete)
        self.label.setObjectName(u"label")
        self.label.setGeometry(QRect(20, 20, 241, 21))
        self.pushButtonViewSummary = QPushButton(DialogComplete)
        self.pushButtonViewSummary.setObjectName(u"pushButtonViewSummary")
        self.pushButtonViewSummary.setGeometry(QRect(146, 60, 101, 31))

        self.retranslateUi(DialogComplete)

        QMetaObject.connectSlotsByName(DialogComplete)
    # setupUi

    def retranslateUi(self, DialogComplete):
        DialogComplete.setWindowTitle(QCoreApplication.translate("DialogComplete", u"Dialog", None))
        self.pushButtonComplete.setText(QCoreApplication.translate("DialogComplete", u"Okay", None))
        self.label.setText(QCoreApplication.translate("DialogComplete", u"<html><head/><body><p align=\"center\"><span style=\" font-size:11pt; font-weight:600;\">Reporting Complete.</span></p></body></html>", None))
        self.pushButtonViewSummary.setText(QCoreApplication.translate("DialogComplete", u"View Summary", None))
    # retranslateUi

