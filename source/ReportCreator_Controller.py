'''
GUI frontend for ReportCreator_Processor.py.
'''

import sys
import os
import copy
import traceback
from datetime import datetime

# Set path for PySide 2 plugins
# Define module constants.
# Get info about this file.
SCRIPT_INTERPRETER_DIR = os.path.dirname(sys.executable)
if getattr(sys, 'frozen', False):
    # Set path for PySide 2 plugins
    qtPluginPath = os.path.join(SCRIPT_INTERPRETER_DIR, r'PySide2\plugins')
else:
    # Set path for PySide 2 plugins
    qtPluginPath = os.path.join(SCRIPT_INTERPRETER_DIR, r'Lib\site-packages\PySide2\plugins')

os.environ['QT_QPA_PLATFORM_PLUGIN_PATH'] = qtPluginPath

from PySide2.QtWidgets import (QMainWindow, QApplication, QStyleFactory, QHeaderView,
                               QTreeWidgetItem, QTreeWidgetItemIterator)
from PySide2.QtCore import Qt, Signal, QRunnable, QThreadPool
from PySide2.QtGui import QIcon, QColor, QBrush

import KV_Imaging as kvimg

from ReportCreator_Window import Ui_MainWindow as MainWindowUi
import ReportCreator_Settings_Controller
import ReportCreator_Complete_Controller
import ReportCreator_Colorbar_Controller
import ReportCreator_Logger_Controller
import ReportCreator_Processor

__version__ = '1.3.3'
__author__ = 'Kinetic Vision, Scott Klausing'
__releasedate__ = 'July 9, 2021'

# Define module constants.
# Get info about this file.
if getattr(sys, 'frozen', False):
    # frozen
    SCRIPT_FULLPATH = sys.executable
else:
    # unfrozen
    SCRIPT_FULLPATH = __file__

SCRIPT_DIRECTORY = os.path.dirname(SCRIPT_FULLPATH)
SCRIPT_FILENAME = os.path.basename(SCRIPT_FULLPATH)

MAIN_DIRECTORY = os.path.dirname(SCRIPT_DIRECTORY)

# Bin folder containing neccesary files for Report Creator to work.
BIN_DIRECTORY = os.path.join(MAIN_DIRECTORY, 'bin')

# Bin folder containing neccesary files for Report Creator to work.
DOCUMENTATION_DIRECTORY = os.path.join(MAIN_DIRECTORY, 'documentation')

# Public AppData folder. Windows users should have write access here.
APP_DATA_DIRECTORY = r'C:\Users\Public\AppData\KV_ReportCreator'
if not os.path.isdir(APP_DATA_DIRECTORY):
    os.makedirs(APP_DATA_DIRECTORY)

# Define icon file for application.
ICON_FILENAME = 'ReportCreator_Small.ico'
ICON_PATH = os.path.join(BIN_DIRECTORY, ICON_FILENAME)
if os.path.isfile(ICON_PATH):
    print(ICON_PATH)
else:
    print("Missing Icon File")

# Define icon file for application.
HELP_DOC_FILENAME = 'KV_ReportCreator_Walkthrough_v%s.pdf' % __version__
HELP_DOC_PATH = os.path.join(DOCUMENTATION_DIRECTORY, HELP_DOC_FILENAME)
if os.path.isfile(HELP_DOC_PATH):
    print(HELP_DOC_PATH)
else:
    print("Missing Documentation File")

# Define user settings file.
SETTINGS_FILENAME = 'Report Creator Main Settings.config'
SETTINGS_FULLPATH = os.path.join(APP_DATA_DIRECTORY, SETTINGS_FILENAME)

KV_IMAGING_DIR = os.path.join(SCRIPT_DIRECTORY, 'KV_Imaging')
sys.path.append(KV_IMAGING_DIR)

DEFAULT_INPUT_DIRECTORY = APP_DATA_DIRECTORY
DEFAULT_REPORT_UNITS = 0  # [mm]


class MainWindow(QMainWindow, MainWindowUi):
    """
    Class for the Report Creator main application window.
    """

    signalAnalysisCurrentFile = Signal(str)
    signalAnalysisCurrentFileStatus = Signal(str)
    signalAnalysisThreadProgress = Signal(int)
    signalAnalysisThreadCancelled = Signal(bool)
    signalAnalysisThreadLog = Signal(str, str)
    signalKeepRunningCode = Signal(str)

    def __init__(self):
        # Initialize the parent classes.
        super().__init__()

        # Initialize the widgets from the ui designer file.
        # NOTE: this adds all of our widgets from the ui file to the MainWindow class.
        self.setupUi(self)

        # Initialize the logging window.
        self.logWindow = ReportCreator_Logger_Controller.LoggingWindow(icon=ICON_PATH)

        # Initilize Logger
        self.init_logger()

        # Initialize the parameter window.
        self.parameterSettingsWindow = ReportCreator_Settings_Controller.ParameterWindow(
            logger=self.settingsLogger,
            icon=ICON_PATH)

        # Initialize the colorbar window.
        self.parameterColorbarWindow = ReportCreator_Colorbar_Controller.ParameterWindow(
            logger=self.colorbarLogger,
            icon=ICON_PATH)

        # Initialize the completion window.
        self.completeWindow = ReportCreator_Complete_Controller.CompleteWindow(
            logger=self.completeLogger,
            logWindow=self.logWindow,
            icon=ICON_PATH)

        # Initialize window properties.
        self.init_window()

        # Initialize window settings.
        self.init_window_settings()

        # Apply window settings.
        self.apply_window_settings()

        # Load and apply settings from file.
        self.load_settings_from_file()

        # Initialize signal actions.
        self.init_signal_actions()

        # Initialize widget actions.
        self.init_widget_actions()

        # Set Progress Bar to 0 to start
        self.progressBar.setProperty("value", 0)

        # Initalize Sample Tree
        self.init_sample_tree()

        return

    def init_logger(self):
        # Initialize Logger in different thread.
        loggerControl = ReportCreator_Logger_Controller.LoggingMain(self.logWindow, 'Controller')
        self.controlLogger = loggerControl.mainLogger

        # Initialize Logger in different thread.
        loggerProcess = ReportCreator_Logger_Controller.LoggingMain(self.logWindow, 'Processor')
        self.processLogger = loggerProcess.mainLogger

        # Get logger from other thread.
        loggerSettings = ReportCreator_Logger_Controller.LoggingMain(self.logWindow, 'Settings')
        self.settingsLogger = loggerSettings.mainLogger

        # Initialize Logger in different thread.
        loggerColorbar = ReportCreator_Logger_Controller.LoggingMain(self.logWindow, 'Colorbar')
        self.colorbarLogger = loggerColorbar.mainLogger

        # Initialize Logger in different thread.
        loggerComplete = ReportCreator_Logger_Controller.LoggingMain(self.logWindow, 'Complete')
        self.completeLogger = loggerComplete.mainLogger

        return

    def init_window(self):
        # Set some options for the window.
        self.setWindowTitle('KV Report Creator - Version %s' % __version__)
        if os.path.isfile(ICON_PATH):
            self.setWindowIcon(QIcon(ICON_PATH))
        else:
            self.controlLogger.warning("Missing Icon File")
        # self.setFixedSize(self.size())
        return

    def init_window_settings(self):
        # Create settings for window.
        self.settingsMain = kvimg.WindowSettings('MainWindow')

        self.settingsMain.defaultInputDirectory = DEFAULT_INPUT_DIRECTORY
        self.settingsMain.userInputDirectory = None

        self.settingsMain.defaultReportUnits = DEFAULT_REPORT_UNITS
        self.settingsMain.userReportUnits = None

        self.allSamplesDict = dict()

        self.allLogWindowText = list()

        runTime = datetime.now()  # current date and time
        self.timeRunStamp = runTime.strftime("%y%m%d_%H%M%S")

        return

    def apply_window_settings(self):

        # Note that the user input doesnt neet set anywhere, just used for default directory when adding
        """ Apply setting to widgets for the window."""
        # Widget: comboBoxReportUnits
        if self.settingsMain.userReportUnits is not None:
            self.comboBoxReportUnits.setCurrentIndex(
                self.settingsMain.userReportUnits)
        else:
            self.comboBoxReportUnits.setCurrentIndex(
                self.settingsMain.defaultReportUnits)

        return

    def load_settings_from_file(self):
        # Load the settings from file.
        if os.path.isfile(SETTINGS_FULLPATH):
            self.settingsMain = kvimg.load_settings_file(SETTINGS_FULLPATH)
            self.apply_window_settings()
            self.controlLogger.info('Main Settings Loaded From File.')
            # # Apply settings for parameter window.
            # self.parameterWindow.settingsParameter = settingsAll.settingsParameter
            # self.parameterWindow.apply_window_settings()
        return

    def store_window_settings(self):

        """ Retrieve the settings from the widgets for the window."""

        self.settingsMain.userReportUnits = self.comboBoxReportUnits.currentIndex()

        return

    def save_settings_to_file(self):
        # Get the latest settings.
        self.store_window_settings()
        # Save the settings file.
        kvimg.save_settings_file(SETTINGS_FULLPATH, self.settingsMain)
        self.controlLogger.info('Main Settings Saved To File.')
        return

    def init_signal_actions(self):
        # Connect the signals to their actions.
        self.signalAnalysisCurrentFile.connect(
            self.update_status_current_filename
        )
        self.signalAnalysisCurrentFileStatus.connect(
            self.update_sample_tree
        )
        self.signalAnalysisThreadCancelled.connect(
            self.analysis_thread_end
        )
        self.signalAnalysisThreadProgress.connect(
            self.update_status_progress_bar
        )

        self.signalAnalysisThreadLog.connect(
            self.process_log_funct
        )
        return

    def init_widget_actions(self):
        # Connect the widgets to their actions.
        self.actionFileExit.triggered.connect(
            self.menu_bar_file_exit
        )
        """ Input Directory Actions"""
        ### Add Directory
        self.pushButtonAddDir.clicked.connect(
            self.select_input_directory_action
        )
        ### Open Selected Directory
        self.pushButtonOpenDir.clicked.connect(
            self.open_selected_tree_dir
        )
        ### Remove Selected Directory
        self.pushButtonRemoveDir.clicked.connect(
            self.remove_selected_tree_dir
        )
        """ Menu Bar Actions """
        ### Exit File Exit Button
        self.actionFileExit.triggered.connect(
            self.menu_bar_file_exit
        )
        ### Exit File New Button
        self.actionNew.triggered.connect(
            self.reset_inputs_action
        )
        ### Edit Settings Defaults Parameters
        self.actionEditDefaults.triggered.connect(
            self.define_settings_action
        )
        ### Edit Settings Colorbar Parameters
        self.actionDeviationSettings.triggered.connect(
            self.define_colorbar_action
        )
        ### Edit Help Documentation Parameters
        self.actionDocumentation.triggered.connect(
            self.display_documentation
        )
        ### Edit Help About Parameters
        self.actionAbout.triggered.connect(
            self.display_app_information
        )
        """ Refresh Action """
        self.pushButtonResetProcessing.clicked.connect(
            self.reset_inputs_action
        )
        """ Cancel Action """
        self.pushButtonCancelProcessing.clicked.connect(
            self.cancel_processing_action
        )
        """ Processing Start Action """
        self.pushButtonStartProcessing.clicked.connect(
            self.start_processing_action
        )
        """ View Log Action """
        self.pushButtonViewLog.clicked.connect(
            self.view_log_action
        )
        """ Change report units """
        self.comboBoxReportUnits.currentIndexChanged.connect(
            self.update_report_units
        )
        return

    def closeEvent(self, event):
        self.save_settings_to_file()
        self.logWindow.close()
        return

    def menu_bar_file_exit(self):
        # NOTE: executes the closeEvent function.
        self.close()
        return

    def display_documentation(self):
        try:
            os.startfile(HELP_DOC_PATH)
        except:
            self.controlLogger.warning('Cannot open the help documentation PDF: %s' % HELP_DOC_PATH)
        return

    def display_app_information(self):
        try:
            message = 'KV Report Creator'
            informText = '  Version: %s \n  Author: %s \n  Release Date: %s' % (
            __version__, __author__, __releasedate__)
            kvimg.alert_user_dialog(message, iconPath=ICON_PATH, informText=informText, detailText=None, logger=None)
        except:
            self.controlLogger.warning('Failed to display application information.')
        return

    def select_input_directory_action(self):
        # Ask user to select directory.
        self.settingsMain.userInputDirectory = kvimg.select_directory_dialog(
            self.settingsMain.userInputDirectory, iconPath=ICON_PATH, logger=self.controlLogger)
        # Update directory.
        if self.settingsMain.userInputDirectory is not None:
            self.update_input_directory()
        return

    def update_input_directory(self):
        if self.settingsMain.userInputDirectory is None or not os.path.isdir(self.settingsMain.userInputDirectory):
            # Update directory line edit widget.
            # self.lineEditInputDirectory.setText('')
            message = 'Please enter existing directory.'
            kvimg.warn_user_dialog(message, iconPath=ICON_PATH, logger=self.controlLogger)
        else:
            # Update directory line edit widget.
            # self.lineEditInputDirectory.setText(strLineEditText)
            message = 'Adding Directory: %s' % self.settingsMain.userInputDirectory
            self.controlLogger.info(message)
            self.settingsMain.inputFolderName = os.path.basename(self.settingsMain.userInputDirectory)
            self.populate_sample_tree(self.settingsMain.userInputDirectory)
        return

    def init_sample_tree(self):
        self.treeWidget.clear()
        # self.treeWidget.setSizeAdjustPolicy(QAbstractScrollArea.AdjustIgnored)
        self.treeHeader = self.treeWidget.header()
        self.treeHeader.setDefaultAlignment(Qt.AlignCenter)
        self.treeHeader.setSectionResizeMode(QHeaderView.Stretch)
        self.treeHeader.setStretchLastSection(False)
        return

    def check_input_already_added(self):
        # Set Tree Widget to auto adjust scrollable area
        self.parameterSettingsWindow.store_window_settings()

        try:
            checkSubDir = self.allSamplesDict[self.settingsMain.inputFolderName]
        except KeyError:
            self.controlLogger.info('New Input Directory Added.')
            return False
        else:
            # Check
            self.controlLogger.warning('Duplicate Input Directory.')
            return True

    def set_input_dictionary(self, inputDirectory, intialSheetFolder, sampleFolders, contentMapperList,
                             headerInfoList, pptTemplateList):

        # Set Added Information to Dictionary
        self.allSamplesDict[self.settingsMain.inputFolderName] = dict()
        self.allSamplesDict[self.settingsMain.inputFolderName]['Project Dir'] = inputDirectory
        self.allSamplesDict[self.settingsMain.inputFolderName]['Initial Sheets'] = intialSheetFolder
        # Content Mapper File
        contentMapper = contentMapperList[0]
        contentMapperName = os.path.basename(contentMapper)
        self.allSamplesDict[self.settingsMain.inputFolderName]['Content Mapper'] = contentMapper

        # Header Info File
        headerInfo = headerInfoList[0]
        headerInfoName = os.path.basename(headerInfo)
        self.allSamplesDict[self.settingsMain.inputFolderName]['Header Info'] = headerInfo

        # Header Info File
        pptTemplate = pptTemplateList[0]
        pptTemplateName = os.path.basename(pptTemplate)
        self.allSamplesDict[self.settingsMain.inputFolderName]['PPT Template'] = pptTemplate

        self.allSamplesDict[self.settingsMain.inputFolderName]['Sample Sheets'] = dict()
        for sampleFolder in sampleFolders:
            sampleName = os.path.basename(sampleFolder)
            self.allSamplesDict[self.settingsMain.inputFolderName]['Sample Sheets'][
                sampleName] = sampleFolder
        return headerInfoName, contentMapperName, pptTemplate

    def populate_sample_tree(self, inputDirectory):

        alreadyAddedInfo = self.check_input_already_added()
        if alreadyAddedInfo:
            message = 'Duplicate Entry!'
            inform = 'Please see details for more information'
            detail = 'Duplicate Input Directory already added: \n%s' % inputDirectory
            # Log is handled by alert_user_dialog
            kvimg.alert_user_dialog(message, informText=inform, detailText=detail, logger=self.controlLogger)
            return

        # Set input Folders
        initialSheetsReq = False
        sampleSheetsReq = False
        samplesExists = False
        intialSheetFolder = None
        sampleFolders = None
        projectReportFolders = kvimg.valid_dirs_in_directory(inputDirectory, logger=self.controlLogger)
        for projectReportFolder in projectReportFolders:
            if 'Static Data' in os.path.basename(projectReportFolder):
                intialSheetFolder = projectReportFolder
                initialSheetsReq = True
            if 'Dynamic Data' in os.path.basename(projectReportFolder):
                sampleSheetFolder = projectReportFolder
                sampleSheetsReq = True
                sampleFolders = kvimg.valid_dirs_in_directory(sampleSheetFolder, logger=self.controlLogger)
                if sampleFolders:
                    samplesExists = True

        # Set input files
        contentMapperList = kvimg.get_files(inputDirectory, ['.xlsx', '.xls'], contains=('content_Mapper', 'content Mapper'),
                                            logger=self.controlLogger)

        headerInfoList = kvimg.get_files(inputDirectory, ['.xlsx', '.xls'], contains=('project_info', 'project info'),
                                         logger=self.controlLogger)

        pptTemplateList = kvimg.get_files(inputDirectory, ['.pptx', '.ppt'], contains=('template'),
                                         logger=self.controlLogger)

        if pptTemplateList is None:
            self.parameterSettingsWindow.store_window_settings()
            pptTemplateFile = self.parameterSettingsWindow.settingsParameter.userPowerpointTemplate
            pptTemplateList = [pptTemplateFile]
            pptTemplateName = '*DEFAULT* %s' % os.path.basename(pptTemplateList[0])
        else:
            pptTemplateName = os.path.basename(pptTemplateList[0])

        if initialSheetsReq and sampleSheetsReq and contentMapperList and headerInfoList and samplesExists:
            headerInfoName, contentMapperName, pptTemplateFile = self.set_input_dictionary(inputDirectory, intialSheetFolder,
                                                                          sampleFolders, contentMapperList,
                                                                          headerInfoList, pptTemplateList)

            varExpImgFile = self.parameterSettingsWindow.settingsParameter.userVarExplainFile
            invalidImgFile = self.parameterSettingsWindow.settingsParameter.userInvalidImgFile

            dirAddedIdx = 0
            pptTemplateIdx = 1
            contentMapperIdx = 2
            headerInfoIdx = 3
            statusIdx = 4

            # Set Tree Items In GUI
            inputTreeItem = QTreeWidgetItem(self.treeWidget)
            inputTreeItem.setText(dirAddedIdx, self.settingsMain.inputFolderName)
            inputTreeItem.setText(pptTemplateIdx, pptTemplateName)
            inputTreeItem.setText(contentMapperIdx, contentMapperName)
            inputTreeItem.setText(headerInfoIdx, headerInfoName)
            inputTreeItem.setText(statusIdx, 'Ready')
            inputTreeItem.setTextAlignment(dirAddedIdx, 4)
            inputTreeItem.setTextAlignment(pptTemplateIdx, 4)
            inputTreeItem.setTextAlignment(contentMapperIdx, 4)
            inputTreeItem.setTextAlignment(headerInfoIdx, 4)
            inputTreeItem.setTextAlignment(statusIdx, 4)
            brush = QBrush()
            brush.setColor(QColor('cyan'))
            brush.setStyle(Qt.BrushStyle.SolidPattern)
            inputTreeItem.setBackground(4, brush)

            for sampleFolder in sampleFolders:
                sampleName = os.path.basename(sampleFolder)
                sampleFolderItem = QTreeWidgetItem(inputTreeItem)
                sampleFolderItem.setText(0, sampleName)
                sampleFolderItem.setTextAlignment(0, 4)

            self.resize_and_expand_tree()
            self.allSamplesDict[self.settingsMain.inputFolderName]['Tree Item'] = inputTreeItem

        else:
            message = 'Missing input files or directories!'
            inform = 'Please see details for more information'
            detail = 'Requirements: \nContent Mapper Excel \nHeader Info Excel \nInitial Sheets Folder \nSample Sheets Folder'
            # Log is handled by alert_user_dialog
            kvimg.alert_user_dialog(message, informText=inform, detailText=detail, logger=self.controlLogger)

        return

    def resize_and_expand_tree(self):
        self.treeWidget.clearSelection()
        it = QTreeWidgetItemIterator(self.treeWidget)
        for item in it:
            treeItem = item.value()
            treeItem.setExpanded(True)
        # self.treeHeader.setSectionResizeMode(QHeaderView.ResizeToContents)
        self.treeHeader.setStretchLastSection(True)
        return

    def get_selected_tree_dir(self):
        parentItem = None
        parentItemText = None
        parentQIndex = None
        parentRow = None
        parentCol = None
        numChildren = None
        currentItem = None
        currentItemText = None
        currentQIndex = None
        currentRow = None
        currentCol = None

        try:
            currentItem = self.treeWidget.currentItem()
            currentItemText = currentItem.text(0)
            currentQIndex = self.treeWidget.indexFromItem(currentItem)
            currentRow = currentQIndex.row()
            currentCol = currentQIndex.column()

            parentItem = currentItem.parent()
            if parentItem:
                parentItemText = parentItem.text(0)
                parentQIndex = self.treeWidget.indexFromItem(parentItem)
                parentRow = parentQIndex.row()
                parentCol = parentQIndex.column()
                numChildren = parentItem.childCount()
        except AttributeError:
            # print('No Tree Item Selected.')
            self.controlLogger.info('No Tree Item Selected.')
        finally:
            return (currentItem, currentItemText, currentQIndex, currentRow, currentCol,
                    parentItem, parentItemText, parentQIndex, parentRow, parentCol, numChildren)

    def open_selected_tree_dir(self):
        # Get the current tree selection
        (currentItem, currentItemText, currentQIndex, currentRow, currentCol,
         parentItem, parentItemText, parentQIndex, parentRow, parentCol, numChildren) = self.get_selected_tree_dir()
        if currentItem is None:
            return

        # Open windows explorer for directory.
        if parentItem is None:
            topItemSelected = True
            directoryPath = self.allSamplesDict[currentItemText]['Project Dir']
            message = 'Opening Project Directory: %s' % currentItemText
            # self.controlLogger.info(message)
        else:
            topItemSelected = False
            directoryPath = self.allSamplesDict[parentItemText]['Sample Sheets'][currentItemText]
            message = 'Opening Sample Directory: %s: %s' % (parentItemText, currentItemText)
            # self.controlLogger.info(message)

        kvimg.open_directory(directoryPath, logger=self.controlLogger)
        return

    def remove_selected_tree_dir(self):
        # Get the current tree selection
        (currentItem, currentItemText, currentQIndex, currentRow, currentCol,
         parentItem, parentItemText, parentQIndex, parentRow, parentCol, numChildren) = self.get_selected_tree_dir()
        if currentItem is None:
            return

        if parentItem is None:
            topItemSelected = True
            self.treeWidget.takeTopLevelItem(currentRow)
            del self.allSamplesDict[currentItemText]
            message = 'Removed Project Directory: %s' % currentItemText
            self.controlLogger.info(message)
        else:
            topItemSelected = False
            if numChildren > 1:
                parentItem.takeChild(currentRow)
                del self.allSamplesDict[parentItemText]['Sample Sheets'][currentItemText]
                message = 'Removed Sample Directory: %s: %s' % (parentItemText, currentItemText)
                self.controlLogger.info(message)
            else:
                self.treeWidget.takeTopLevelItem(parentRow)
                del self.allSamplesDict[parentItemText]
                message = 'Removed Project Directory: %s' % parentItemText
                self.controlLogger.info(message)
        return

    def reset_inputs_action(self):
        """ Apply setting to widgets for the window."""
        # self.update_input_directory()
        self.progressBar.setProperty("value", 0)
        self.init_sample_tree()
        self.allSamplesDict = dict()
        self.controlLogger.info('Inputs Reset...')
        self.reset_thread_widget_status()
        self.logWindow.plainTextEditLogger.clear()

    def define_settings_action(self):
        # Open parameters window.
        self.parameterSettingsWindow.show()
        self.parameterSettingsWindow.activateWindow()
        self.settingsLogger.info('Defining Settings...')
        return

    def define_colorbar_action(self):
        # Open parameters window.
        self.parameterColorbarWindow.show()
        self.parameterColorbarWindow.activateWindow()
        self.colorbarLogger.info('Defining Colorbar Options.')
        return

    def view_log_action(self):
        self.logWindow.show()
        self.logWindow.activateWindow()
        return

    def save_log_action(self, logType='Project'):
        if logType == 'Project':
            self.controlLogger.info('Project %s: Log Saved.' % self.currentFileProcessing)
            logWindowText = self.logWindow.plainTextEditLogger.toPlainText()
            kvimg.save_log(self.currentLogFile, message=logWindowText)
            self.allLogWindowText.append(logWindowText)
            self.logWindow.plainTextEditLogger.clear()
        elif logType == 'Main':
            self.controlLogger.info('Main Run %s: Log Saved.' % self.timeRunStamp)
            logWindowText = self.logWindow.plainTextEditLogger.toPlainText()
            finalLogText = '\n----------\n'.join(self.allLogWindowText)
            kvimg.save_log(self.mainLogFile, message=finalLogText)
            self.logWindow.plainTextEditLogger.clear()
            self.logWindow.plainTextEditLogger.setPlainText(finalLogText)
        elif logType == 'Append':
            logWindowText = self.logWindow.plainTextEditLogger.toPlainText()
            self.allLogWindowText.append(logWindowText)
            self.logWindow.plainTextEditLogger.clear()

        return

    def start_thread_widget_status(self):
        self.pushButtonStartProcessing.setEnabled(False)
        self.pushButtonResetProcessing.setEnabled(False)

        self.pushButtonAddDir.setEnabled(False)
        self.pushButtonOpenDir.setEnabled(False)
        self.pushButtonRemoveDir.setEnabled(False)

        self.menubar.setEnabled(False)
        self.pushButtonCancelProcessing.setEnabled(True)

        self.resize_and_expand_tree()
        self.comboBoxReportUnits.setEnabled(False)

    def end_thread_widget_status(self):
        self.pushButtonStartProcessing.setEnabled(False)
        self.pushButtonResetProcessing.setEnabled(True)

        self.pushButtonAddDir.setEnabled(False)
        self.pushButtonOpenDir.setEnabled(True)
        self.pushButtonRemoveDir.setEnabled(False)

        self.menubar.setEnabled(False)
        self.pushButtonCancelProcessing.setEnabled(False)

        self.resize_and_expand_tree()
        self.comboBoxReportUnits.setEnabled(False)

    def cancel_thread_widget_status(self):
        self.pushButtonStartProcessing.setEnabled(False)
        self.pushButtonResetProcessing.setEnabled(False)

        self.pushButtonAddDir.setEnabled(False)
        self.pushButtonOpenDir.setEnabled(False)
        self.pushButtonRemoveDir.setEnabled(False)

        self.menubar.setEnabled(False)
        self.pushButtonCancelProcessing.setEnabled(False)

        self.resize_and_expand_tree()
        self.comboBoxReportUnits.setEnabled(False)

    def reset_thread_widget_status(self):
        self.pushButtonStartProcessing.setEnabled(True)
        self.pushButtonResetProcessing.setEnabled(True)

        self.pushButtonAddDir.setEnabled(True)
        self.pushButtonOpenDir.setEnabled(True)
        self.pushButtonRemoveDir.setEnabled(True)

        self.menubar.setEnabled(True)
        self.pushButtonCancelProcessing.setEnabled(False)

        self.resize_and_expand_tree()
        self.comboBoxReportUnits.setEnabled(True)

    def update_report_units(self):
        self.settingsMain.userReportUnits = self.comboBoxReportUnits.currentIndex()
        return

    def update_sample_tree(self, status):
        if self.currentFileProcessing is not None:
            self.currentTreeItem = self.allSamplesDict[self.currentFileProcessing]['Tree Item']
            self.currentTreeItem.setText(4, status)
            if status == 'Processing':
                brush = QBrush()
                brush.setColor(QColor('yellow'))
                brush.setStyle(Qt.BrushStyle.SolidPattern)
                self.currentTreeItem.setBackground(4, brush)
            elif status == 'Error':
                brush = QBrush()
                brush.setColor(QColor('red'))
                brush.setStyle(Qt.BrushStyle.SolidPattern)
                self.currentTreeItem.setBackground(4, brush)
            elif status == 'Cancelled':
                brush = QBrush()
                brush.setColor(QColor('red'))
                brush.setStyle(Qt.BrushStyle.SolidPattern)
                self.currentTreeItem.setBackground(4, brush)
            elif status == 'Completed':
                brush = QBrush()
                brush.setColor(QColor('#00ff00'))
                brush.setStyle(Qt.BrushStyle.SolidPattern)
                self.currentTreeItem.setBackground(4, brush)
                self.save_log_action()
        return

    def cancel_processing_action(self):
        self.signalKeepRunningCode.emit(False)
        self.controlLogger.warning('Canceling report generator.')
        self.cancel_thread_widget_status()
        return

    def start_processing_action(self):
        # Disable the main gui while processing.
        # self.setEnabled(False)
        self.start_thread_widget_status()
        self.controlLogger.info('Starting report generator computation.')

        self.threadpool = QThreadPool()
        print("Multithreading with maximum %d threads" % self.threadpool.maxThreadCount())

        # Create analysis thread.
        threadAnalysis = AnalysisThread(
            self.signalAnalysisCurrentFile,
            self.signalAnalysisCurrentFileStatus,
            self.signalAnalysisThreadProgress,
            self.signalAnalysisThreadCancelled,
            self.signalAnalysisThreadLog,
            self.signalKeepRunningCode)

        # Store inputs
        threadAnalysis.sampleDictionary = copy.copy(self.allSamplesDict)

        self.parameterColorbarWindow.store_window_settings()
        threadAnalysis.cmapObj = copy.copy(self.parameterColorbarWindow.settingsColorbar.colorMap)
        threadAnalysis.normObj = copy.copy(self.parameterColorbarWindow.settingsColorbar.colorNorm)
        threadAnalysis.maxNominal = copy.copy(self.parameterColorbarWindow.settingsColorbar.maxNominalControllerUser)
        threadAnalysis.minNominal = copy.copy(self.parameterColorbarWindow.settingsColorbar.minNominalControllerUser)
        threadAnalysis.maxCritical = copy.copy(self.parameterColorbarWindow.settingsColorbar.maxCriticalControllerUser)
        threadAnalysis.minCritical = copy.copy(self.parameterColorbarWindow.settingsColorbar.minCriticalControllerUser)

        self.parameterSettingsWindow.store_window_settings()
        # threadAnalysis.pptTemplateFile = copy.copy(
        #     self.parameterSettingsWindow.settingsParameter.userPowerpointTemplate)
        threadAnalysis.varExpImgFile = copy.copy(self.parameterSettingsWindow.settingsParameter.userVarExplainFile)
        threadAnalysis.invalidImgFile = copy.copy(self.parameterSettingsWindow.settingsParameter.userInvalidImgFile)

        self.update_report_units()
        threadAnalysis.projectUnits = copy.copy(self.settingsMain.userReportUnits)

        self.mainLogFile = os.path.join(APP_DATA_DIRECTORY, '%s.log' % self.timeRunStamp)
        self.save_log_action(logType='Append')

        # Start analysis thread.
        self.threadpool.start(threadAnalysis)
        return

    def analysis_thread_end(self, cancel=False):
        self.update_status_current_filename(None)
        self.end_thread_widget_status()
        if not cancel:
            self.controlLogger.info('Report generator finished.')
            self.update_status_progress_bar(100)
            self.completeWindow.setWindowTitle('Process Complete.')
            self.completeWindow.label.setText('Process Complete.')
            self.completeWindow.show()
            self.completeWindow.activateWindow()
        else:
            self.controlLogger.info('Report generator cancelled.')
            self.completeWindow.setWindowTitle('Process Cancelled.')
            self.completeWindow.label.setText('Process Cancelled.')
            self.completeWindow.show()
            self.completeWindow.activateWindow()
        self.save_log_action(logType='Main')
        return

    def update_status_current_filename(self, strFilename):
        # Update the Qlabel for the current file that is processing.

        if strFilename is not None:
            self.currentFileProcessing = strFilename

            logSaveDir = self.allSamplesDict[self.currentFileProcessing]['Project Dir']
            logSaveName = '%s.log' % self.currentFileProcessing
            self.currentLogFile = os.path.join(logSaveDir, logSaveName)
            self.save_log_action()

            self.labelStatusCurrentFilename.setText(logSaveDir)
        else:
            self.labelStatusCurrentFilename.setText('Complete')

    def update_status_progress_bar(self, percentComplete):
        # Update the Qlabel for the current file that is processing.
        self.progressBar.setProperty("value", percentComplete)
        return

    def process_log_funct(self, text, level):
        if level == 'info':
            self.processLogger.info(text)
        elif level == 'error':
            self.processLogger.error(text)
        else:
            self.processLogger.warning(text)
        return


# threading.Thread
class AnalysisThread(QRunnable):
    """
    Class that handles processing the analysis routine in its own thread.
    """

    def __init__(self, signalAnalysisFile, signalAnalysisFileStatus,
                 signalAnalysisThreadProgress, signalAnalysisThreadCancelled,
                 signalAnalysisThreadLog, signalKeepRunningCode):
        # Initialize the parent class.
        super().__init__()
        # Set thread name.
        self.name = 'AnalysisThread'

        # Initialize variables.
        self.signalAnalysisFile = signalAnalysisFile
        self.signalAnalysisFileStatus = signalAnalysisFileStatus
        self.signalAnalysisProgress = signalAnalysisThreadProgress
        self.signalAnalysisCancelled = signalAnalysisThreadCancelled
        self.signalAnalysisThreadLog = signalAnalysisThreadLog
        self.signalKeepRunningCode = signalKeepRunningCode

        self.isAnalysisSuccess = False

        self.sampleDictionary = None

        self.cmapObj = None
        self.normObj = None
        self.maxCritical = None
        self.minCritical = None
        self.maxNominal = None
        self.minNominal = None

        self.pptTemplateFile = None
        self.varExpImgFile = None
        self.invalidImgFile = None

        self.projectUnits = None

        self.keepRunning = True

        self.signalKeepRunningCode.connect(
            self.run_check
        )

        return

    def run(self):
        print('\nAnalysis thread started.')
        self.signalAnalysisThreadLog.emit('Analysis thread started.', 'info')

        # Use try/except statement to catch an unexpected errors during analysis.
        # This ensures the app does not crash entirely during use.
        try:
            # Execute the analysis routine.
            self.analysis_routine()
        except:
            errorTraceback = traceback.format_exc()
            self.signalAnalysisThreadLog.emit('Error encountered during analysis routine. %s' % errorTraceback, 'error')
            # print('\nError encountered during analysis routine.')
            # print(errorTraceback)
        else:
            self.isAnalysisSuccess = True

        # print('\nAnalysis thread finished.')
        self.signalAnalysisThreadLog.emit('Analysis thread finished.', 'info')
        return

    def run_check(self, keepRunning):
        self.keepRunning = keepRunning
        return

    def analysis_routine(self):

        fileNum = 1

        colorMapObj = self.cmapObj
        colorMapNorm = self.normObj

        maxCritical = self.maxCritical
        minCritical = self.minCritical

        varExpImgFile = self.varExpImgFile
        invalidImgFile = self.invalidImgFile

        projectUnits = self.projectUnits

        numofProjects = len(self.sampleDictionary)

        # Batch process mesh files in input directory.
        for projectIdx, (projectFolderName, projectFolderInfo) in enumerate(self.sampleDictionary.items()):
            projectDir = projectFolderInfo['Project Dir']
            initialSheetsDir = projectFolderInfo['Initial Sheets']
            contentMapFile = projectFolderInfo['Content Mapper']
            headerInfoFile = projectFolderInfo['Header Info']
            pptTemplateFile = projectFolderInfo['PPT Template']
            sampleDirs = projectFolderInfo['Sample Sheets']

            self.signalAnalysisFile.emit(projectFolderName)
            self.signalAnalysisFileStatus.emit("Processing")

            if not self.keepRunning:
                # Emit the process complete signal.
                self.signalAnalysisFileStatus.emit("Cancelled")
                self.signalAnalysisCancelled.emit(True)
                continue

            try:
                self.signalAnalysisThreadLog.emit('Starting Project: %s' % projectFolderName, 'info')
                reportCreator = ReportCreator_Processor.ReportCreator(self.signalAnalysisThreadLog, self.signalAnalysisFileStatus)
                reportCreator.projectDirectory = projectDir
                reportCreator.initialSheetsDirectory = initialSheetsDir
                reportCreator.sampleDirPaths = sampleDirs
                reportCreator.contentMapperFile = contentMapFile
                reportCreator.headerInfoFile = headerInfoFile
                reportCreator.pptTemplate = pptTemplateFile
                reportCreator.varianceExpImgFile = varExpImgFile
                reportCreator.noImgFoundFile = invalidImgFile
                reportCreator.cmapObj = colorMapObj
                reportCreator.normObj = colorMapNorm
                reportCreator.maxCritical = maxCritical

                reportCreator.main_process()

            except:
                print('\nError encountered processing file:\n%s.' % projectFolderName)
                # Clean up the ReReport instance.
                ReReport = None
                fileNum += 1
            else:
                percentComplete = int(((projectIdx + 1) / numofProjects) * 100)
                self.signalAnalysisProgress.emit(percentComplete)

        if self.keepRunning:
            self.signalAnalysisCancelled.emit(False)

        return


if __name__ == '__main__':
    # Add support for multiprocessing for when code has been frozen as Windows executable.
    # https://docs.python.org/2/library/multiprocessing.html#miscellaneous
    # freeze_support()

    # Enable dpi scaling so if the windows system setting for scale is large than 100%
    # the application window scales accordingly without getting distorted.
    QApplication.setAttribute(Qt.AA_EnableHighDpiScaling, on=True)

    # Look at this line
    QApplication.setAttribute(Qt.AA_UseHighDpiPixmaps, on=True)

    # Start the application.
    print('\nApplication started.')
    app = QApplication(sys.argv)

    # Set the main window style. Use windowsvista for newest windows style.
    # print(QStyleFactory.keys())
    app.setStyle(QStyleFactory.create('windowsvista'))
    # app.setStyle(QStyleFactory.create('Windows'))
    # app.setStyle(QStyleFactory.create('Fusion'))

    # Initialize and show the main window.
    mainGui = MainWindow()
    mainGui.show()
    app.exit(app.exec_())
    print('\nApplication closed.')
